<?php
require  '../__connect_db.php';
$page_name = 'room_data_list';
$page_title = '教室資料列表';

$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
$search = isset($_GET['search']) ? $_GET['search'] : '';        //search box

//升降冪
$up="up";
$down="down";
$change=isset($_GET['change'])?$_GET['change']:'';

$per_page = 10; //每一頁要顯示幾筆

$params = [];
$where = ' WHERE 1 ';
if (!empty($search)) {
    $params['search'] = $search;
    $search1 = $pdo->quote("%$search%");
    $where .= " AND (`name` LIKE $search1 OR cost LIKE $search1 OR contain LIKE $search1) ";
}

$count = "SELECT COUNT(1) FROM class_room $where"; //用count計算出總筆數
$totalRows = $pdo->query($count)->fetch(PDO::FETCH_NUM)[0];
if($totalRows==0){
    $where = ' WHERE 1 ';
    $count = "SELECT COUNT(1) FROM class_room $where";
    $totalRows = $pdo->query($count)->fetch(PDO::FETCH_NUM)[0];
}

$totalPages = ceil($totalRows / $per_page);


if ($page < 1) {                                              //小於1轉回第一頁
    header('Location: room_data_list.php?page=1'.'&search='.$search);
    exit;
}
if ($page > $totalPages) {
    header('Location: room_data_list.php?page=' . $totalPages .'&search='.$search);   //大於總頁數，轉回最後一頁
    exit;
}

if($search == '' ){


    if($change==$down){
        
        $sql = sprintf(
            // "SELECT * FROM class_room ORDER BY room_sid DESC LIMIT %s, %s",
            // ($page - 1) * $per_page,
            // $per_page
            "SELECT * FROM `class_room` cr , `country` ct where cr.country_sid = ct.country_sid ORDER BY cr.room_sid DESC LIMIT %s, %s",
            ($page-1)*$per_page,
            $per_page
        );

    }else{
       
        $sql = sprintf(
            // "SELECT * FROM class_room ORDER BY room_sid ASC LIMIT %s, %s",
            // ($page - 1) * $per_page,
            // $per_page
            "SELECT * FROM `class_room` cr , `country` ct where cr.country_sid = ct.country_sid ORDER BY cr.room_sid ASC LIMIT %s, %s",
            ($page-1)*$per_page,
            $per_page
            
        );

    }
       
    $stmt = $pdo->query($sql);
}else{      
        $sql="";
        if($change==$down){
           
            $sql = "SELECT * FROM `class_room` cr, `country` ct $where AND cr.country_sid = ct.country_sid  ORDER BY room_sid DESC LIMIT " . ($page - 1) * $per_page . "," . $per_page;
            // $sql = "SELECT * FROM restaurant $where ORDER BY restaurant_id DESC LIMIT " . ($page - 1) * $per_page . "," . $per_page;
        }
        else{
            $sql = "SELECT * FROM `class_room` cr, `country` ct $where AND cr.country_sid = ct.country_sid  ORDER BY room_sid ASC LIMIT " . ($page - 1) * $per_page. "," . $per_page;
            // $sql = "SELECT * FROM restaurant $where ORDER BY restaurant_id ASC LIMIT " . ($page - 1) * $per_page . "," . $per_page;
        }
        
        $stmt = $pdo->query($sql);
        
}



// $t_sql = "SELECT count(1) FROM `class_room`";

// $t_stmt = $pdo->query($t_sql);


// $totalRows = $pdo->query($t_sql)->fetch(PDO::FETCH_NUM)[0]; // 拿到總筆數
// $totalPages = ceil($totalRows/$per_page);

// if($page < 1){
//     header('Location: room_data_list.php');
//     exit;
// }
// if($page > $totalPages){
//     header('Location: room_data_list.php?page='. $totalPages);
//     exit;
// }

$sql = sprintf("SELECT * FROM class_room cr , country ct where cr.country_sid = ct.country_sid ORDER BY cr.room_sid DESC LIMIT %s, %s",
    ($page-1)*$per_page,
    $per_page
);


// $stmt = $pdo->query($sql);

//$rows = $stmt->fetchAll();

?>
<style>
    .small-img{
        height:50px;
        width:50px;
    }

</style>

<?php include  '../__html_head.php' ?>
<?php include  '../__html_body.php' ?>
<style>
    tbody tr.active {
            background: lightblue;
        }
</style> 
       
<div class="container">
<div class="row d-flex justify-content-end" style="margin-right: 20px; margin-bottom: 20px">
    <div class="col-2 ">
    <form>
    <div class="input-group no-border ">
        <input type="text" name="search" value="" class="form-control" placeholder="Search...">
        <div class="input-group-append">
            <div class="input-group-text">
                <!-- <i class="nc-icon nc-zoom-split"></i> -->
            </div>
        </div>
    </div>
</form>
    </div>
</div>
    <div style="margin-top: 2rem;">
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item">
                    <a class="page-link" href="?page=<?= $page-1 ?>&search=<?=$search?>&change<?=$change?>">
                        <i class="fas fa-chevron-left"></i>
                    </a>
                </li>
                <?php
                $p_start = $page-5;
                $p_end = $page+5;
                for($i=$p_start; $i<=$p_end; $i++):
                    if($i<1 or $i>$totalPages) continue;
                    ?>
                    <li class="page-item <?= $i==$page ? 'active' : '' ?>">
                        <a class="page-link" href="?page=<?= $i ?>&search=<?=$search?>&change<?=$change?>"><?= $i ?></a>
                    </li>
                <?php endfor; ?>
                <li class="page-item">
                    <a class="page-link" href="?page=<?= $page+1 ?>&search=<?=$search?>&change<?=$change?>">
                        <i class="fas fa-chevron-right"></i>
                    </a>
                </li>
            </ul>
        </nav>



        <div class="card">           
        <table class="table table-bordered">
            <thead>
            <tr>
                <th scope="col" style="vertical-align:left;">
                                        <label class='checkbox-inline checkboxeach'>
                                            <input id='checkAll' type='checkbox' name='checkboxall' value='1'></label>選取
                                    </th>    
                <th scope="col">編輯</i></th>
                <th scope="col"><a href="room_data_list.php?change=<?php if($change==$up or $change==''){echo $down;}  ?>&search=<?=$search?>&page=<?= $page ?>"><i class="fas fa-sort-amount-up-alt"></i></a></th>
                <th scope="col">縣市</th>
                <th scope="col">行政區</th>
                <th scope="col">區域</th>
                <th scope="col">圖片</th>
                <th scope="col">教室名稱</th>
                <th scope="col">電話</th>
                <th scope="col">費用</th>
                <th scope="col">可容納人數</th>
                <th scope="col">地址</th>
                <th scope="col">刪除</th>

            </tr>
            </thead>
            <tbody>
            <?php while($r=$stmt->fetch()){  ?>
                <tr>
                <td class="box_td">
                                            <label class=' checkbox-inline checkboxeach'>
                                                <input id="<?= 'readtrue' . $r['room_sid'] ?>" type='checkbox' name=<?= 'readtrue' . $r['room_sid'] . '[]' ?> value='<?= $r['room_sid'] ?>'> <!-- 選取框 -->
                                            </label>
                                        </td>
                    <td><a href="room_data_edit.php?sid=<?=$r['room_sid'] ?>"><i class="fas fa-edit"></i></a>
                    </td>
                    <td><?= $r['room_sid'] ?></td>
                    <td><?= $r['city'] ?></td>
                    <td><?= $r['dist'] ?></td>
                    <td><?= $r['Orientation'] ?></td>
                    <td><a href="<?= htmlentities($r['room_images']) ?>" class="small-img" data-lightbox="image-<?= htmlentities($r['room_sid']) ?>" data-title="<?= htmlentities($r['name']) ?>"><img class="small-img" src="<?= htmlentities($r['room_images']) ?>"></div></td>
                    <td><?= htmlentities($r['name']) ?></td>
                    <td><?= htmlentities($r['phone']) ?></td>
                    <td><?= htmlentities($r['cost']) ?></td>
                    <td><?= htmlentities($r['contain']) ?></td>
                    <td><?= htmlentities($r['address']) ?></td>
                    <td>
                        <a href="javascript:delete_one(<?= $r['room_sid'] ?>)"><i class="fas fa-trash-alt"></i></a></td>
                </tr>
            <?php } ?>

        
            </tbody>
        </div> 
        </table>
    </div>

    <script>
        function delete_one(sid) {
            if(confirm(`確定要刪除編號為 ${sid} 的資料嗎?`)){
                location.href = 'room_data_delete.php?sid=' + sid;
            }
        }
    </script>
<script>
    let checkAll = $('#checkAll'); //控制所有勾選的欄位
    let checkBoxes = $('tbody .checkboxeach input'); //其他勾選欄位

    checkAll.click(function() {
        for (let i = 0; i < checkBoxes.length; i++) {
            checkBoxes[i].checked = this.checked;
        }
    })
</script>
<script>
    //可顯示勾選列顏色

      let dataCount=$("tbody tr").length;
        console.log(dataCount);


      $("tbody :checkbox").click(function(){
          let checked = $(this).prop("checked")
          console.log(checked);
          let checkedCount=$("tbody :checked").length;
          console.log(checkedCount);

          if(checked){
              $(this).closest("tr").addClass("active")
          }else{
              $(this).closest("tr").removeClass("active")
          }
          if(dataCount==checkedCount){
                $("#checkAll").prop("checked", true)
            }else{
                $("#checkAll").prop("checked", false)
            }

      })



      $("#checkAll").click(function () {
            let checkAll = $(this).prop("checked");
            // console.log(checkAll);

            $("tbody :checkbox").prop("checked", checkAll);
            if(checkAll){
                $("tbody tr").addClass("active");
            }else{
                $("tbody tr").removeClass("active");
            }
        })
  </script>
</div>
<?php include  '../__html_foot.php' ?>
