<?php
require '../__admin_required.php';
require '../__connect_db.php';
$page_name = 'farmer';
$page_title = '小農';
$page_d = array(
    'name' => 'farmer',
    'title' => '小農',
    'file' => 'Farmers',
    'type' => 'list',
    'parent' => 'adminHome',
    'parent_title' => 'Home'
);

?>
<?php include '../__html_head.php' ?>
    <style>
        #trash:hover{
            color: #ff3f41
        }
        #edit:hover{
            color: #3087ff
        }
        .transition{
            transition: .5s;
        }
        .card_select{
            transition: .3s;
        }
        .card_select.active{

            /*border: 5px outset #cc6968;*/
            transform: translate(-2px,-5px);
            box-shadow: 0 0 20px 8px rgb(94, 122, 94);
        }
        .card-user .image img {
            width: 100%;
            height: 100%;
            object-fit: cover;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }
        .card-user .avatar {
            width: 124px;
            height: 124px;
            object-fit: cover;
            border: 1px solid #FFFFFF;

        }
        .rounded-top.farmer-modal {
            border-top-left-radius: 12px !important;
            border-top-right-radius: 12px !important;
        }
    </style>
<?php include '../__html_body.php' ?>
    <div class="col-md-6 col-lg-7 col-sm-6 ml-2 d-inline-block">
        <div class="row justify-content-end align-items-center">
            <div class="form-group" style="display:flex ; margin:0">

                <input type="text" class="form-control search_move" id="search_value" name="search_value"
                       placeholder="Value" style="margin:0 10px; transition: 0.5s;">
            </div>

            <a id="search_advance" style="line-height:50% transition: 0.5s;" hidden><i class="fas fa-lg fa-search"></i></a>
            <a id="search_search" style="line-height:50% transition: 0.5s;"><i class="fas fa-lg fa-search"></i></a>

        </div>
    </div>

    <div class="content mt-n2" id="content" >
        <div id="content_row" class="row justify-content-center">
            <div class="col-md-12">
                <div class="card" id="main_card">
                    <div class="card-header row position-relative">
                        <h4 class="card-title ml-4">Members</h4>
                        <a href="farmer_create.php" id="farmer_create" class="ml-1" >
                        <span class="fa-stack fa-lg" style="transform: translateY(25%)">
                            <i class="far fa-circle fa-stack-2x"></i>
                            <i class="fas fa-plus fa-stack-1x"></i>
                        </span>
                        </a>
                        <a class="position-absolute" href="javascript: delete_all()" style="top:20px ;right: 70px;"><i id="trash" class="fas fa-trash fa-stack-2x transition" style="color: #f6f5f5"></i></a>

                    </div>
                    <div class="card-body pt-1">
                        <div class="">
                            <table class="table">
                                <thead class=" text-primary">
                                <tr>
                                    <th><div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input hidden class="form-check-input" type="checkbox" id="checkall">
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div></th>
                                    <th><a id="id_desc" href="javascript: false;">ID<i id="icon_desc"
                                                                                       class="fas fa-caret-up"></i></a>
                                    </th>
                                    <th>StoreName</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Password</th>
                                    <th>Mobile</th>
                                    <th class="text-center">Edit</th>
                                    <th class="text-center">Del</th>
                                </tr>
                                </thead>
                                <tbody id="t_content">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <nav aria-label="Page navigation position-relative" id="test1">
            <div id="pageText" class="position-absolute" style="left: 50px;"><p style="color: #9f9f9f"></p></div>
            <ul class="pagination justify-content-center">
            </ul>
        </nav>
    </div>
    </div>
    </div>

<?php include __DIR__ . '/__js_script.php' ?>

    <script>


        $('#content_row').on('click','#checkall',function (e) {
            e.stopPropagation()
            if($('#checkall:checked').prop("checked")){
                $(".checkall").prop("checked",true);
                animateCSS('#trash', 'bounce');
            }else{
                 $('#trash').css("color",'#f6f5f5');
                $(".checkall").prop("checked",false);
            }
        })

        $('#content_row').on('click','.checkall',function(e){
            e.stopPropagation()
            console.log($(".checkall:checked").length)
            console.log($(".checkall").length)
            if($(this).prop("checked")){
                animateCSS('#trash', 'bounce');
            }
            if($(".checkall:checked").length==0){
                $('#trash').css("color",'#f6f5f5');
            }
            if($(".checkall:checked").length==$(".checkall").length){
                $("#checkall").prop("checked",true);
            }else{
                $("#checkall").prop("checked",false);
            }


        })
        // $("input[name='farmerId[]']:checked").map(function() { return $(this).val(); }).get();
        function delete_all(){
            let delete_all = [];
            if($(":checked").length!==0) {

                delete_all = $("input[name='farmerId[]']:checked").map(function () {
                    return $(this).val();
                }).get();
                console.log(delete_all);
                delete_all_api(delete_all)
            }
            if($('#content_row .card_select.active').length!==0){
                $('#content_row .card_select.active').each(function(){delete_all.push($(this).attr('data-farmer'))})
                console.log(delete_all);
                delete_all_api(delete_all)
            }else{
                $('#trash_card').css("color",'#d0d0d0');
            }

        }
        function delete_all_api(id) {

            let info = "Delete";

            let page = pagination_active;
            Notiflix.Confirm.Show(
                '! WARNING !',
                'Are You Sure ?',
                'Delete',
                'Go Back',
                // ok button callback
                function() {
                    let action_date = new Date();
                    let action_time = `${action_date.getHours()}:${action_date.getMinutes()}`;
                    addnotice(info, id, action_time);
                    bell_notice.addClass("bell_notice_active");
                    $('#trash_card').css("color",'#d0d0d0');
                    loadData(page,id)
                    console.log(id);
                },
                // cancel button callback
                function() {
                    console.log('no'+id);
                    $('#trash').css("color",'#f6f5f5');
                    $('#trash_card').css("color",'#d0d0d0');
            $(":checked").prop("checked",false);
                    $('.card_select').removeClass('active')

                }
            );
        }

    </script>
<?php include __DIR__ . '/__js_addnotice.php' ?>
<?php include '../__html_foot.php' ?>