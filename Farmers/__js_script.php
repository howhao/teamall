<script>
    // $(window).scroll(function () {
    //     console.log($(this).scrollTop())
    // })
    function animateCSS(element, animationName, callback) {
        const node = document.querySelector(element)
        node.classList.add('animated', animationName)
        node.setAttribute("style", "color:#ff3f41")

        function handleAnimationEnd() {
            node.classList.remove('animated', animationName)
            node.removeEventListener('animationend', handleAnimationEnd)
            // node.setAttribute("style","color:#f6f5f5")

            if (typeof callback === 'function') callback()
        }

        node.addEventListener('animationend', handleAnimationEnd)
    }

    const layout_swich = $('#layout_swich');
    const bell_active = $('#navbarDropdownMenuLink');
    const bell_notice = $('#bell_notice');
    const search_value = $('#search_value');
    const search_advance = $('#search_advance');
    const search_search = $('#search_search');
    let pagination_active = 0;
    var list_card_swich = 0;
    var id_desc = document.querySelector('#id_desc');
    const pagination = document.querySelector('.pagination');
    const t_content = document.querySelector('#t_content');
    const content_row = document.querySelector('#content_row');
    const sort_ud = [
        'ID<i id="icon_desc" class="fas fa-caret-up"></i>',
        'ID<i id="icon_desc" class="fas fa-caret-down"></i>'
    ];
    let sort_v = 0;
    const pagination_str = `
            <li class="page-item <%= active %>">
                <a class="page-link" href="javascript:loadData(<%= i %>)"><%= i %></a>
            </li>
        `;
    const pagination_str1 = `
            <li class="page-item <%= active %>">
                <a class="page-link" href="javascript:searchData(<%= i %>,<%= v %>)"><%= i %></a>
            </li>
        `;

    const table_row_str = `
        <tr>
                <td><div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input hidden name="farmerId[]" class="form-check-input checkall" type="checkbox" id="check<%= farmer_id %>" value="<%= farmer_id %>">
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div></td>
                <td><%= farmer_id %></td>
                <td><div class="demo-area">
    <a  href="" id="stopsort" data-toggle="modal" data-target="#farmerModal<%= farmer_id %>"><%= storename %></a>
</div>
<div class="modal fade" id="farmerModal<%= farmer_id %>" tabindex="-1" role="dialog" aria-labelledby="demoModal" style="display: none;" aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered " role="document">
            <div class="modal-content">
                <button type="button" class="close light" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
               <div class="">
                   <div class="bg-img m-h-30 rounded-top farmer-modal" style="background-image: url('../assets/img/farm1.jpg')">

                   </div>
                   <div class="pull-up-lg">
                       <div class="container-fluid">
                           <div class="row">
                               <div class="col-md-10 mx-auto pb-5">
                                   <div class="bg-light rounded shadow-sm px-3 py-3">
                                       <h2 class="pt-sm-3  text-center"><%= storename %></h2>
                                       <p class="text-muted text-center">
                                           <%= aboutme %>
                                       </p>
                                       <form class="px-sm-4 py-sm-4 row">
                                        <p class="col-6"><span class="description">Company: </span> <%= company %></p>
                                        <p class="col-6"><span class="description">Taxid: </span> <%= taxid %></p>
                                        <p class="col-6"><span class="description">Name: </span> <%= name %></p>
                                        <p class="col-6 "><span class="description">Mobile: </span> <%= mobile %></p>
                                        <p class="col-12 text-center"><span class="description">Email: </span> <%= email %></p>
                                        <p class="col-6"><span class="description">Password: </span> <%= password %></p>
                                        <p class="col-6"><span class="description">Address: </span> <%= address %></p>
                                           <button id="modal_edit" type="submit" class="btn btn-cstm-dark btn-block btn-cta" data-farmermodal="<%= farmer_id %>">Edit <i class="fas fa-pen"></i></button>
                                       </form>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
            </div>
        </div>
    </div>
</td>
                <td><%= name %></td>
                <td><%= email %></td>
                <td><%= password %></td>
                <td><%= mobile %></td>
                <td class="text-center"><a id="edit" href="farmer_edit.php?sid=<%= farmer_id %>"><i class="fas fa-pen"></i></a></td>
                <td class="text-center"><a id="trash" href="javascript:delete_one(<%= farmer_id %>)"><i class="fas fa-trash"></i></a></td>
            </tr>
        `;
    const list_head = $('#content_row').html();
    //test
    const trash_can = `<a class="position-absolute" href="javascript: delete_all()" style="top: 85px;
                right: 50%; transform: translateX(-14px);"><i id="trash_card" class="fas fa-trash fa-stack-2x transition"
                style="color: #d0d0d0"></i></a>`;
    const card_row_str = `
    <div class="col-md-3">
    <div class="card card-user card_select" id="farmer<%= farmer_id %>" data-farmer="<%= farmer_id %>">
        <div class="image">
                <img src="./uploads/<%= _.dropRight(image)%>" alt="...">
        </div>
        <div class="card-body">
            <div class="author">
                <a class="" >
                    <img class="avatar border-gray"
                         src="./uploads/<%= _.drop(image)%>" alt="...">
                </a><h5 class="title">
                <a class="farmer_link" href="farmer_edit.php?sid=<%= farmer_id %>">
                    <%= storename %>

                </a></h5>
                <p class="description">
                    <%= name %>
                </p>
            </div>
            <p class="description text-center">
                <%= email %>
            </p>
            <p class="description text-center">
                    <%= mobile %>
                </p>
        </div>
        <div class="card-footer">
            <hr>
            <div class="button-container">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-6 ml-auto">
                        <h5>12
                            <br>
                            <small>Product</small>
                        </h5>
                    </div>
                    <div class="col-lg-4 col-md-6 col-6 ml-auto mr-auto">
                        <h5>20
                            <br>
                            <small>Follow</small>
                        </h5>
                    </div>
                    <div class="col-lg-4 mr-auto">
                        <h5>30k
                            <br>
                            <small>Month</small>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        `;
    const card_row_fn = _.template(card_row_str);
    //test

    const pagination_fn = _.template(pagination_str);
    const pagination_fn1 = _.template(pagination_str1);
    const table_row_fn = _.template(table_row_str);
    const row_fn = [table_row_fn, card_row_fn];
    let sort = 'farmer_api.php?page=';

    bell_active.click(function () {
        bell_notice.removeClass("bell_notice_active");
    });
    Notiflix.Confirm.Init({
        width: "350px",
        okButtonBackground: "#ce4e4e",
        titleColor: "#e81616",
        titleFontSize: "20px",
        fontFamily: "Arial",
        useGoogleFont: false,
    });

    function delete_one(id) {
        $("input:checked").prop("checked", false);
        $('#check' + id).click()
        let info = "Delete";

        let page = pagination_active;
        Notiflix.Confirm.Show(
            '! WARNING !',
            'Are You Sure ?',
            'Delete',
            'Go Back',
            // ok button callback
            function () {
                let action_date = new Date();
                let action_time = `${action_date.getHours()}:${action_date.getMinutes()}`;
                addnotice(info, id, action_time);
                bell_notice.addClass("bell_notice_active");
                $('#trash').css("color", '#f6f5f5');
                loadData(page, id);
            },
            // cancel button callback
            function () {
                console.log(id);
                $('#trash').css("color", '#f6f5f5');
                $("input:checked").prop("checked", false);

            }
        );
    }

    var t1, t2, t3, t4, t5, t6, t7;
    var search_status = true;

    function search() {
        window.clearTimeout(t2);
        window.clearTimeout(t3);
        window.clearTimeout(t4);
        console.log('cleartime');
        let page = 1;
        let id = 0;
        let value = search_value.val();
        console.log(search_status + ' search');
        searchData(page, value);
        t5 = setTimeout(() => {
            search_search.removeAttr("href");
        }, 100);
        t6 = setTimeout(() => {
            search_value.addClass("search_move");
        }, 8500);
        t7 = setTimeout(() => {
            search_status = true;
            search_search.removeAttr("hidden");
        }, 9000);
    }

    search_search.click(() => {
        if (search_status) {
            console.log(search_status + ' begin');
            search_action();

        }
    })


    function search_action() {
        console.log(search_status + ' action');
        search_status = false;
        console.log(search_status + ' change');
        search_value.removeClass("search_move");
        t1 = setTimeout(() => {
            search_search.attr("href", 'javascript:search()');
            console.log(' press now');
        }, 100);

        t2 = setTimeout(() => {
            search_search.attr("hidden", '');
        }, 15000);
        t3 = setTimeout(() => {
            search_value.addClass("search_move");
        }, 16500);
        t4 = setTimeout(() => {
            search_status = true;
            search_search.removeAttr("href");
            search_search.removeAttr("hidden");
        }, 17000);
    }


    function searchData(page = 1, value) {
        $('#trash').css("color", '#f6f5f5');
        $(":checked").prop("checked", false);
        console.log(value);
        fetch(`farmer_search_api.php?page=${page}&value=${value}`)
            .then(response => {
                console.log(response);
                return response.json()
            })
            .then(json => {
                console.log(json.rows);
                let i, s, item;
                let t_str = '';
                for (s in json.rows) {
                    item = json.rows[s];
                    t_str += row_fn[0](item);
                }
                console.log(item);
                t_content.innerHTML = t_str;

                let p_str = '';
                for (i = 1; i <= json.totalPages; i++) {
                    let active = i === json.page ? 'active' : '';
                    let v = json.value;
                    p_str += pagination_fn1({
                        i: i,
                        active: active,
                        v: v
                    });
                }
                $('#pageText').find('p').html("Total " + json.totalRows + " ")
                pagination.innerHTML = p_str;
                pagination_active = document.querySelector('ul.pagination li.active a').innerHTML;
            })

    }

    let list_icon = [
            `<i class="nc-icon-outline design_bullet-list-67" >
                             </i>`, `<i class="nc-icon nc-layout-11 " >
                             </i>`
        ]
    ;
    layout_swich.on('click', function () {
        let page_s = $('.pagination li.active a').text()
        if (list_card_swich === 0) {
            $('a#layout_swich').html(list_icon[list_card_swich])
            list_card_swich = 1;
            loadData(page_s);
        } else {
            $('a#layout_swich').html(list_icon[list_card_swich])
            list_card_swich = 0;
            content_row.innerHTML = list_head;
            id_desc = document.querySelector('#id_desc');
            loadData(page_s);
        }

    })
let image_default = ['farm1.jpg','farmer1.jpg']
    function loadData(page = 1, id) {
        $('#trash').css("color", '#f6f5f5');
        $("input:checked").prop("checked", false);
        if (!!id) {
            console.log(id);
            fetch('farmer_delete_api.php?sid=' + id);
        }
        fetch('farmer_api.php?page=' + page + "&sort=" + sort_v)
            .then(response => {
                return response.json();
            })
            .then(json => {
                console.log(json.rows);
                let i, s, item,new_img;
                let t_str = '';
                for (s in json.rows) {
                    item = json.rows[s];
                    if(!item['img']){
                        item.image = image_default;
                    }else{
                        new_img = JSON.parse(item['img']);
                        item.image = new_img;
                    }

                    t_str += row_fn[list_card_swich](item);
                }
                console.log(item);
                if (list_card_swich === 0) {
                    $("#content_row").sortable({
                        opacity: 0.7,
                    });
                    $("#content_row").sortable('disable')
                    let t_content = document.querySelector('#t_content');
                    console.log(list_card_swich)
                    t_content.innerHTML = t_str;
                    $("#t_content").sortable({
                        handle: 'label',
                        axis: "y",
                        opacity: 0.7,
                    });
                } else {
                    content_row.innerHTML = trash_can;
                    content_row.innerHTML += t_str;
                    $("#content_row").sortable('enable')

                }


                let p_str = '';
                for (i = 1; i <= json.totalPages; i++) {
                    let active = i === json.page ? 'active' : '';
                    p_str += pagination_fn({
                        i: i,
                        active: active
                    });
                }
                $('#pageText').find('p').html("Total " + json.totalRows + " ");
                pagination.innerHTML = p_str;
                pagination_active = document.querySelector('ul.pagination li.active a').innerHTML;



            });

    }

    loadData();


    const test = (el) => {
        console.log('123');
        let page_s = $('.pagination li.active a').text()
        if (sort == 'farmer_api_desc.php?page=') {
            id_desc.innerHTML = sort_ud[0];
            sort_v = 0;
            sort = 'farmer_api.php?page=';
            loadData(page_s);
        } else {
            if ((el.target.id == 'id_desc') | (el.target.id == 'icon_desc')) {
                id_desc.innerHTML = sort_ud[1];
                sort_v = 1;
                sort = 'farmer_api_desc.php?page='
                loadData(page_s);
            } else {
                console.log(el.target);
                sort = 'farmer_api.php?page=';
                alert('only ASC');
            }
        }

    }
    $('#content_row').on('click', '#id_desc', test)

    $('#content_row').on('click', '.card_select', function () {
        if ($(this).hasClass('active')) {

        } else {
            animateCSS('#trash_card', 'bounce');
        }
        $(this).toggleClass('active')

        if ($('#content_row .card_select.active').length == 0) {
            $('#trash_card').css("color", '#d0d0d0');
        }
    })
    $('#content_row').on('click', '.card .farmer_link', function (e) {
        e.stopPropagation()
        $('.card').removeClass('active')
    })
    //$('#farmer_create').click(function(e){
    //    e.preventDefault()
    //    let text = `<?php //include __DIR__ . '/__js_farmer_create.php' ?>//`;
    //    $('#content').html(text);
    //})

    // id_desc.addEventListener('click', test);
    $('#content_row').on('click', '#modal_edit', function (e) {
        e.preventDefault()
        let farmerId = $(this).data('farmermodal')
        // console.log(farmerId)
        location.href = `farmer_edit.php?sid=${farmerId}`
    })
    $('#content_row').on('click', '#stopsort', function () {
        let show = $('.modal.fade').hasClass('show')
        // console.log(show)
        if(!show){
            $("#t_content").sortable('disable')
        }
    })
    $('#content_row').on('click', '.modal.fade', function () {
        let show = $('.modal.fade').hasClass('show')
        // console.log(show)
        if(!show) {
            $("#t_content").sortable('enable')
        }
    })
</script>