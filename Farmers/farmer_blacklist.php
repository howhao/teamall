<?php
require '../__admin_required.php';
require '../__connect_db.php';
$page_name = 'farmer_blacklist';
$page_title = '黑名單';
//$page_a = array(
//    'name' => 'farmer_blacklist',
//    'title' => '黑名單',
//    'file' => 'Farmers',
//    'type' => 'list',
//    'parent' => 'adminHome',
//    'parent_title' => 'Home'
//);

$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
$per_page = 10;//每頁幾筆
$t_sql = "SELECT COUNT(1) FROM `farmers` WHERE `status`=0";//資料幾筆
$t_stmt = $pdo->query($t_sql);
$totalRows = $t_stmt->fetch(PDO::FETCH_NUM)[0];//設為索引陣列,索引0為總筆數
$totalPages = ceil($totalRows / $per_page);//總頁數

//if ($page < 1) {
//    header('Location: farmer.php');
//    exit;
//}
//if ($page > $totalPages) {
//    header('Location: farmer.php?page=' . $totalPages);
//    exit;
//}
$result = [
    'page' => $page,
    'per_page' => $per_page,
    'totalRows' => $totalRows,
    'totalPages' => $totalPages,
    'rows' => [],
];

$sql = sprintf("SELECT * FROM `farmers` WHERE `status`=0 ORDER BY `farmer_id` ASC LIMIT %s, %s",
    ($page - 1) * $per_page,
    $per_page
);//(從第幾筆開始),(選擇幾筆)
$stmt = $pdo->query($sql);
$rows = $stmt->fetchAll();

?>
<?php include '../__html_head.php' ?>
    <style>

        #verify_confirm:hover{
            color: #3087ff
        }
        #verify_deny:hover{
            color: #ff3f41
        }
    </style>
<?php include '../__html_body.php' ?>
    <div class="col-md-6 col-lg-7 col-sm-6 ml-2 d-inline-block">
        <div class="row justify-content-end align-items-center">
            <div class="form-group" style="display:flex ; margin:0">

                <input type="text" class="form-control search_move" id="search_value" name="search_value"
                       placeholder="Value" style="margin:0 10px; transition: 0.5s;">
            </div>

            <a id="search_advance" style="line-height:50% transition: 0.5s;" hidden><i class="fas fa-lg fa-search"></i></a>
            <a id="search_search" style="line-height:50% transition: 0.5s;"><i class="fas fa-lg fa-search"></i></a>

        </div>
    </div>
    <div class="content mt-n2">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header row">
                        <h4 class="card-title ml-4">黑名單</h4>
                    </div>
                    <div class="card-body pt-1">
                        <div class="">
                            <table class="table">
                                <thead class=" text-primary">
                                <tr>
                                    <th><a id="id_desc" href="javascript: sort(0)">ID<i id="icon_desc" class="fas fa-caret-up"></i></a>
                                    </th>
                                    <th>Company</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Password</th>
                                    <th>Mobile</th>
                                    <th class="text-center">Approved</th>
<!--                                    <th class="text-center">Deny</th>-->
                                </tr>
                                </thead>
                                <tbody id="t_content">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <nav aria-label="Page navigation" id="test1">
            <ul class="pagination justify-content-center">
            </ul>
        </nav>
    </div>
    </div>
    </div>
    <script>
        const bell_active = $('#navbarDropdownMenuLink');
        const bell_notice = $('#bell_notice');
        const pagination = document.querySelector('.pagination');
        const t_content = document.querySelector('#t_content');
        const id_desc = document.querySelector('#id_desc');
        const sort_ud = [
            'ID<i id="icon_desc" class="fas fa-caret-up"></i>',
            'ID<i id="icon_desc" class="fas fa-caret-down"></i>'
        ];
        const pagination_str = `
            <li class="page-item <%= active %>">
                <a class="page-link" href="javascript:loadData(<%= i %>)"><%= i %></a>
            </li>
        `;
        const table_row_str = `
        <tr>
                <td><%= farmer_id %></td>
                <td><%= company %></td>
                <td><%= name %></td>
                <td><%= email %></td>
                <td><%= password %></td>
                <td><%= mobile %></td>
                <td class="text-center"><a id="verify_confirm" href="javascript: verify(<%= farmer_id %>,2)"><i class="fas fa-check fa-lg"></i></i></a></td>
<!--                <td class="text-center"><a id="verify_deny" href="javascript: verify(<%= farmer_id %>,0)"><i class="fas fa-ban fa-lg"></i></a></td>-->
            </tr>
        `;

        const pagination_fn = _.template(pagination_str);

        const table_row_fn = _.template(table_row_str);
        function verify(id,status){
            fetch('farmer_blacklist_verify_api.php?id='+ id +'&status=' + status)
                .then(response => {
                    console.log(response);
                    return response.json();
                })
                .then(json => {
                    console.log(json);
                    if(json.success){
                        let page = pagination_active;
                        loadData(page);
                    }
                })
        }

        var sort_v = 0 ;

        function sort(s){
            let page = pagination_active;

            let sort;
            if(id_desc.innerHTML==sort_ud[0]){
                sort = 0;
                console.log(sort)
                sort_v = 1;
                id_desc.innerHTML=sort_ud[1];
                id_desc.href = "javascript: sort(1)";
            }else if(id_desc.innerHTML==sort_ud[1]){
                console.log(sort)
                sort = 1;
                sort_v = 0;
                id_desc.innerHTML=sort_ud[0];
                id_desc.href = "javascript: sort(0)";
            }
            loadData(page);
        }

        function loadData(page = 1){
            console.log(sort_v)
            fetch('farmer_blacklist_api.php?page='+ page + '&sort=' + sort_v)
                .then(response => {
                    console.log(response);
                    return response.json();
                })
                .then(json => {
                    console.log(json);
                    console.log(json.sort);
                    let i, s, item;
                    let t_str = '';
                    for (s in json.rows) {
                        item = json.rows[s];
                        t_str += table_row_fn(item);
                    }
                    // console.log(item);
                    t_content.innerHTML = t_str;

                    let p_str = '';
                    for (i = 1; i <= json.totalPages; i++) {
                        let active = i === json.page ? 'active' : '';
                        p_str += pagination_fn({
                            i: i,
                            active: active
                        });
                    }
                    pagination.innerHTML = p_str;
                    pagination_active = document.querySelector('ul.pagination li.active a').innerHTML;

                });

        }
        loadData();

    </script>

<?php include '../__html_foot.php' ?>