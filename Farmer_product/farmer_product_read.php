<?php require '../__admin_required.php'?>
<?php require '../__connect_db.php' ?>
<?php
$page_name = "farmer_product_read";
$page_title ='商品清單';
$page_d = array(
  'name' => 'farmer_product_read',
  'title' => '商品清單',
  'file' => 'Farmer_product',
  'type' => 'list',
  'parent' => 'adminHome',
  'parent_title' => 'Home'
);


$value = isset($_GET['search_value']) ? $_GET['search_value']: '';
$page = isset($_GET['page']) ? intval($_GET['page']) : 1;

$per_page = 10; // 每一頁要顯示幾筆

//排序升降冪
$up="up";
$down="down";
$change=isset($_GET['change'])?$_GET['change']:'';

//測試
$created_at="created_at";
$price="price";
$stock="stock";
$shelves="shelves";
$name="name";
$sel=isset($_GET['sel'])?$_GET['sel']:'';
//搜尋
$where = " WHERE 1";
if (!empty($value)) {
    // $params['value'] = $value;
    $value1 = $pdo->quote("%$value%");
    $where .= " AND (`name` LIKE $value1 OR `price` LIKE $value1 OR `writing` LIKE $value1 OR `content` LIKE $value1) ";
}

$t_sql = "SELECT COUNT(1) FROM `farmer_product` d JOIN `farmers` p ON d.farmer_sid=p.farmer_id $where";
$t_stmt = $pdo->query($t_sql);


$totalRows = $t_stmt->fetch(PDO::FETCH_NUM)[0]; // 拿到總筆數

$notice="查無搜尋結果";


if($totalRows==0){
    header('Location:farmer_product_read.php?notice=' . "查無搜尋結果" );
    
    exit;
}

// $t_sql = "SELECT COUNT(1) FROM `farmer_product`";


// $t_stmt = $pdo->query($t_sql);
// $totalRows = $t_stmt->fetch(PDO::FETCH_NUM)[0]; // 拿到總筆數
//$totalRows = $pdo->query($t_sql)->fetch(PDO::FETCH_NUM)[0]; // 拿到總筆數

$totalPages = ceil($totalRows/$per_page); // 取得總頁數

//echo "$totalRows <br>";
//echo "$totalPages <br>";
//exit;
if($page<1){
  header('Location: farmer_product_read.php?page=' . 1 .'&search_value=' . $value  );
  exit;  //直接離開不在跑下面
}
if($page>$totalPages){
    header('Location: farmer_product_read.php?page='. $totalPages. '&search_value=' . $value);
    exit;
  }

//小農名單加產品名單

if($value =='' ){
        
  if($change==$down){

    if(!$sel==""){
      $sql = sprintf(
        "SELECT d.*,p.`farmer_id`,p.`name` farmer_name FROM `farmer_product` d JOIN `farmers` p ON d.farmer_sid=p.farmer_id  ORDER BY `$sel` DESC LIMIT %s, %s",
        ($page - 1) * $per_page,
        $per_page
    );
    }else{
      $sql = sprintf(
      "SELECT d.*,p.`farmer_id`,p.`name` farmer_name FROM `farmer_product` d JOIN `farmers` p ON d.farmer_sid=p.farmer_id  ORDER BY `sid` DESC LIMIT %s, %s",
      ($page - 1) * $per_page,
      $per_page
    );
    } 

  }else{

    if(!$sel==""){
      $sql = sprintf(
        "SELECT d.*,p.`farmer_id`,p.`name` farmer_name FROM `farmer_product` d JOIN `farmers` p ON d.farmer_sid=p.farmer_id  ORDER BY `$sel` ASC LIMIT %s, %s",
        ($page - 1) * $per_page,
        $per_page
    );
    }else{
      $sql = sprintf(
      "SELECT d.*,p.`farmer_id`,p.`name` farmer_name FROM `farmer_product` d JOIN `farmers` p ON d.farmer_sid=p.farmer_id  ORDER BY `sid` ASC LIMIT %s, %s",
      ($page - 1) * $per_page,
      $per_page
    );
    } 
   
  }
  // $sql = sprintf(
  //     "SELECT d.*,p.`farmer_id`,p.`name` farmer_name FROM `farmer_product` d JOIN `farmers` p ON d.farmer_sid=p.farmer_id ORDER BY `sid` LIMIT %s, %s",
  //     ($page - 1) * $per_page,
  //     $per_page
  // );
  $stmt = $pdo->query($sql); 
}else{ 
  
  if($change==$down){

    if(!$sel==""){
      $sql = "SELECT d.*,p.`farmer_id`,p.`name` farmer_name FROM `farmer_product` d JOIN `farmers` p ON d.farmer_sid=p.farmer_id $where ORDER BY `$sel` DESC LIMIT " . ($page - 1) * $per_page . "," . $per_page;    
    }else{
      $sql = "SELECT d.*,p.`farmer_id`,p.`name` farmer_name FROM `farmer_product` d JOIN `farmers` p ON d.farmer_sid=p.farmer_id ORDER BY `sid` DESC LIMIT " . ($page - 1) * $per_page . "," . $per_page;
    } 
}else{
  if(!$sel==""){
    $sql = "SELECT d.*,p.`farmer_id`,p.`name` farmer_name FROM `farmer_product` d JOIN `farmers` p ON d.farmer_sid=p.farmer_id ORDER BY `$sel` ASC LIMIT " . ($page - 1) * $per_page . "," . $per_page;    
  }else{
    $sql = "SELECT d.*,p.`farmer_id`,p.`name` farmer_name FROM `farmer_product` d JOIN `farmers` p ON d.farmer_sid=p.farmer_id ORDER BY `sid` ASC LIMIT " . ($page - 1) * $per_page . "," . $per_page;
  } 

}  
    // $sql = "SELECT * FROM `farmer_product` $where ORDER BY `name` DESC LIMIT " . ($page - 1) * $per_page . "," . $per_page;
    $stmt = $pdo->query($sql);
}

// tag的選項取出
$t_sql = "SELECT `sid`, `name` FROM `tag`";
$t_stmt = $pdo->query($t_sql);
$tRows = $t_stmt->fetchALL();


$tagname="TAG名";

// if($value =='' ){
        
//   $sql = sprintf(
//       "SELECT * FROM `farmer_product` LIMIT %s, %s",
//       ($page - 1) * $per_page,
//       $per_page
//   );
//   $stmt = $pdo->query($sql); 
// }else{      
//     $sql = "SELECT * FROM `farmer_product` $where ORDER BY `name` DESC LIMIT " . ($page - 1) * $per_page . "," . $per_page;
//     $stmt = $pdo->query($sql);
// }


// $sql = sprintf("SELECT d.*,p.`farmer_id`,p.`name` farmer_name FROM `farmer_product` d JOIN `farmers` p ON d.farmer_sid=p.farmer_id ORDER BY `sid` LIMIT %s, %s",
//         ($page-1)*$per_page,
//             $per_page
// );
// $stmt = $pdo->query($sql);




?>
<?php include '../__html_head.php' ?>
<?php include '../__html_body.php' ?>

<style>
  @keyframes shake {
  from,
  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }

  5%,
  15%,
  25%,
  35%,
  45%,
  55%,
  75%,
  95% {
    -webkit-transform: translate3d(-1px, 0.5px, 0);
    transform: translate3d(-1px, 0.5px, 0);
  }
  10%,
  20%,
  30%,
  50%,
  70%,
  80%,
  90% {
    -webkit-transform: translate3d(1px, -0.5px, 0);
    transform: translate3d(1px, -0.5px, 0);
  }
}
*{
  /* border:1px solid red; */
}
.box_pic{
  width:150px;
  height:120px;
  overflow: hidden;
  margin:0 5px;
  border-radius:5px;
}
.pic{
  width: 100%;
  height:100%;  
  object-fit: cover;
  object-position: center;
}
.green{
  background: #5eced5;
}
.white{
  background: #fff;
}
.red{
  color: pink;
}
.gray{
  background: rgba(10,10,10,0.7);
  border-radius:5px;
}
.tagstyle{
  background: #5eced5;
  border-radius:2px;
  color:white;
  padding:5px;
  margin:3px;
}
.fz2{
  font-size:16px;
}
.fz3{
  font-size:18px;
}
.fz5{
  font-size:40px;
}
.greenblue{
  background: #5eced5;
  border-radius:2px;
}

</style>

<!-- div -->
<div class="content mt-0 mb-2" >

      <div class="mb-2 d-flex justify-content-between row col-md-12">



       <!-- 頁數按鈕 -->
<nav aria-label="Page navigation example " >
  <ul class="pagination justify-content-center">
  <li class="page-item">
    <a class="page-link" href="?page=1">
    <i class="fas fa-angle-double-left"></i></a></li>
  <li class="page-item">
    <a class="page-link" href="?page=<?=$page-1?>&search_value=<?=$value?>&change=<?=$change?>">
    <i class="fas fa-chevron-left"></i></a></li>
  <?php 
  $p_start=$page-3;
  $p_end=$page+3;
  for($i=$p_start; $i<=$p_end;$i++): 
  if($i<1 or $i>$totalPages)continue;
  ?>
    <li class="page-item <?= $i==$page? 'active' : '' ?>">
    <a class="page-link" href="?page=<?= $i ?>&search_value=<?=$value?>&change=<?=$change?>"><?= $i?></a></li>
<?php endfor; ?>
    <li class="page-item">
    <a class="page-link" href="?page=<?=$page+1?>&search_value=<?=$value?>&change=<?=$change?>">
    <i class="fas fa-chevron-right"></i></a></li>
    <li class="page-item">
    <a class="page-link" href="?page=<?=$totalPages?>">
    <i class="fas fa-angle-double-right"></i></a></li>
  </ul>
</nav>
            <div class="row align-items-center">

<!-- 搜尋BAR -->
            
               <input type="text" class="form-control" id="search_value" name="search_value"
                placeholder="<?=isset($_GET['notice'])?$_GET['notice']: ''?>" style="margin:0 10px; transition: 0.5s; width:150px;z-index:100;">
               <a id="search_search" style="line-height:50% transition: 0.5s;" href="javascript:search()"><i class="fas fa-search fa-2x mt-1"></i></a>     
            </div>
         

    </div>

                    <!-- <form>
                      <div class="row justify-content-center">
                        <div class="input-group no-border col-md-4">
                            <input type="text"  class="form-control" name="search_value" placeholder="">
                            <div class="input-group-append">
                            <a id="search_search" style="line-height:50% transition: 0.5s;" href="javascript:search()"><i class="fas fa-search fa-2x mt-1"></i></a> 
                                <div class="input-group-text">
                                   
                                </div>
                            </div>
                         </div>
                      </div>
                        
                    </form> -->

  <!-- 展示列表 -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header row d-flex justify-content-between align-items-center">
              <div class="row"> <h4 class="card-title ml-5">商品資訊</h4>
                <a href="farmer_product_insert.php" class="ml-1">
                       <span class="fa-stack fa-lg" style="transform: translateY(25%)">
                            <i class="far fa-circle fa-stack-2x"></i>
                            <i class="fas fa-plus fa-stack-1x"></i>
                        </span>
                </a></div>

                <div class="ml-5 ">查詢資料總筆數:<?=$totalRows?> </div>
               
                <div class="mr-5 row d-flex align-items-center">
                <a href="javascript:delete_all()"><i class="fas fa-trash-alt fa-2x mr-3 delete_all"></i></a>
                <a href="javascript:onobj_all()"><i class="fas fa-cart-plus fa-2x mr-3 onobj_all"></i></a>
                <a href="javascript:outobj_all()"><i class="fas fa-cart-arrow-down fa-2x outobj_all"></i></a>
                </div>
                
              </div>

              <div class="card-body">
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                      <th scope="col" style="vertical-align:left;">
                        <label class='checkbox-inline checkboxeach form-check'>
                            <input id='checkAll' type='checkbox' name='checkboxall' value='1' class="form-check-inputform-check-input">
                            <span class="form-check-sign mr-4 green"></span>
                            </label>
                            
                      </th>
                      <!-- <th><input type="checkbox">全選</th> -->
                      <th>編號</th>
                      <th>小農名</th>
                      <th>商品名<a href="farmer_product_read.php?sel=<?php if($sel==$name or $sel==$price or $sel==$shelves or $sel==$stock or $sel==$created_at or $sel==''){echo $name;} ?>&change=<?php if($change==$up or $change==''){echo $down;}  ?>&search_value=<?=$value?>&page=<?= $page ?>"><i class="fas fa-arrows-alt-v"></i></a></th>
                      <th>圖片</th>
                      <th>價格<a href="farmer_product_read.php?sel=<?php if($sel==$name or $sel==$price or $sel==$shelves or $sel==$stock or $sel==$created_at or $sel==''){echo $price;} ?>&change=<?php if($change==$up or $change==''){echo $down;}  ?>&search_value=<?=$value?>&page=<?= $page ?>"><i class="fas fa-arrows-alt-v"></i></a></th>
                      <th>庫存<a href="farmer_product_read.php?sel=<?php if($sel==$name or $sel==$price or $sel==$shelves or $sel==$stock or $sel==$created_at or $sel==''){echo $stock;} ?>&change=<?php if($change==$up or $change==''){echo $down;}  ?>&search_value=<?=$value?>&page=<?= $page ?>"><i class="fas fa-arrows-alt-v"></i></a></th>
                      <th>上傳日期 <a href="farmer_product_read.php?sel=<?php if($sel==$name or $sel==$price or $sel==$shelves or $sel==$stock or $sel==$created_at or $sel==''){echo $created_at;} ?>&change=<?php if($change==$up or $change==''){echo $down;}  ?>&search_value=<?=$value?>&page=<?= $page ?>"><i class="fas fa-arrows-alt-v"></i></a></th>
                      <th>狀態 <a href="farmer_product_read.php?sel=<?php if($sel==$name or $sel==$price or $sel==$shelves or $sel==$stock or $sel==$created_at or $sel==''){echo $shelves;} ?>&change=<?php if($change==$up or $change==''){echo $down;}  ?>&search_value=<?=$value?>&page=<?= $page ?>"><i class="fas fa-arrows-alt-v"></i></a></th>
                      <th>顯示更多</th>
                      <th>編輯</th>
                      <th>刪除</th>
                      
                    </thead>
                    <tbody id="sortable">
                   
                      
                    <?php while($r=$stmt->fetch()){  ?>
                      <tr class="white"> 
                      <td> <label class=' checkbox-inline checkboxeach form-check '>
                                <input id="<?= 'readtrue' . $r['sid'] ?>" type='checkbox' name=<?= 'readtrue' . $r['sid'] . '[]' ?> value='<?= $r['sid'] ?>'> <!-- 選取框 -->
                                <span class="form-check-sign mr-4 green"></span>
                      </label> </td>
                   <!-- <td> <input type="checkbox" ></td> -->
                    <?php  $pk=$r['sid'] ?>
                   <td><?= $r['sid'] ?></td>
                   <td><?= $r['farmer_name'] ?></td>
                   <?php  $nk=$r['name'] ?>
                   <td><?= $r['name'] ?></td>
                   <td  class="d-flex"> 
                     <?php
                  //  $r['picture']
                    $pic=json_decode($r['picture'], JSON_UNESCAPED_UNICODE);
                    if(isset($pic)){
                       
                      foreach($pic as $phot){
                        // echo " <img src=uploads/"."$phot" . " , "."\""."alt=\"\" width=\"150\">" ;
                        echo "<a href=\"uploads/"."$phot"."\""."data-lightbox=\"$pk\" data-title=\"$nk\"><div class=\"box_pic\"><img src=uploads/"."$phot" . " , "."\""."alt=\"\" class=\"pic\" ></div></a>" ;
                      }
                    } 
                    
                    ?></td>
                   <td><?= $r['price'] ?></td>
                   <?php if($r['stock']<=10 and $r['stock']>0 ){$co_stock="text-danger font-weight-bold";}else if($r['stock']==0){$co_stock="text-danger font-weight-bold border border-danger";}else{$co_stock="text-dark";} ?>
                   <td class="<?=$co_stock?> text-center"><?= $r['stock'] ?></td>
                   <td><?= $r['created_at'] ?></td>
                   <?php if($r['shelves']==0){
                     $onout="下架中" ;$color_onout="text-danger";}
                     else{$onout="上架中" ;$color_onout="text-success";} ?>
                   <td class="<?=$color_onout?>"><?= $onout ?></td>
                   <td>    
                      <a class="demo-area">
                          <i class="fas fa-ellipsis-h fa-2x text-info pl-2" data-toggle="modal" data-target="#m<?= $r['sid'] ?>"></i>
                      </a>
                  </td>
                   <td>
                     <a href="javascript:edit_one(<?= $r['sid'] ?>)"><i class="fas fa-pen"></i></a>&nbsp;&nbsp;
                       
                   </td>
                   <td>
                     <a href="javascript:delete_one(<?= $r['sid'] ?>)"><i class="fas fa-trash-alt "></i></a>
                   </td>
 <!-- 內文光箱 -->     
 <div class="modal fade "   id="m<?= $r['sid'] ?>"  tabindex="-1" role="dialog"
         aria-labelledby="m<?= $r['sid'] ?>" aria-hidden="true">
        <div class="modal-dialog  modal-lg modal-dialog-centered " role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
               <div class="container-fluid">
                   <div class="row">
                   <?php
                  //  $r['picture']
                    $pic=json_decode($r['picture'], JSON_UNESCAPED_UNICODE);
                    if(isset($pic)){
                       
                      foreach($pic as $phot){
                        // echo " <img src=uploads/"."$phot" . " , "."\""."alt=\"\" width=\"150\">" ;
                        // echo "<a href=\"uploads/"."$phot"."\""."data-lightbox=\"$pk\" data-title=\"$nk\"><div class=\"box_pic\"><img src=uploads/"."$phot" . " , "."\""."alt=\"\" class=\"pic\" ></div></a>" ;
                      }
                    } 
                    
                    ?>
                       <div class="col-md-6 bg-img d-none d-sm-flex align-items-end"  
                       style="background-image: url('uploads/<?= $phot ?>')">
                           <div class="pb-5 pt-5 text-white">
                              <div class="pt-2 px-2 gray fz2 mt-5 mb-0 overflow-hidden"> <span class="fz5 ">❝</span> <br> <div class="mt-3"><?= $r['writing'] ?> </div><br><span class="fz5 float-right">❞</span></div>
                               <h5 class="py-2 greenblue px-3 mb-5"><span class="fz5">-</span><?= $r['farmer_name'] ?></h5>
                           </div>
                       </div>
                       <div class="col-md-6 py-5 px-sm-5 mt-5">
                           <h3>#<?= $r['sid'] ?>  <span class="ml-5"><?= $r['name'] ?></span></h3>
                           <form>
                               <div class="form-row">
                                   <div class="form-group col-md-6 ">
                                       <div class="text-info fz3">小標:</div>
                                       <div class="pt-1"><?= $r['subtitle'] ?></div>
                                   </div>
                                   <div class="form-group col-md-6">
                                       <div class="text-info fz3">規格:</div>
                                       <div class="pt-1"><?= $r['specification'] ?></div>
                                   </div>
                               </div>
                               <div class="form-row">
                                   <div class="form-group col-md-6 ">
                                       <div class="text-info fz3">顏色:</div>
                                       <div class="pt-1"><?= $r['color'] ?></div>
                                   </div>
                                   <div class="form-group col-md-6">
                                       <div class="text-info fz3">產地:</div>
                                       <div class="pt-1"><?= $r['place'] ?></div>
                                   </div>
                               </div>                               
                               <div class="form-group">
                                   <div class="text-info fz3">內容:</div>
                                   <div class="pt-1"><?= $r['content'] ?></div>
                               </div>
                               <div class="form-row">
                                   <div class="form-group col-md-6">
                                     <div class="text-info fz3 ">標籤TAG: </div>                   
                                   </div>
                                </div>
                                <div class="form-group col-md-12">
                                      <div class="d-flex"> 
                                      <?php  $newtaged = json_decode($r['tag_sid'], JSON_UNESCAPED_UNICODE);
                                       foreach($newtaged as $k_tag){
                                        switch($k_tag){
                                             case 1:
                                          $tagname="有機";
                                             break;
                                             case 2:
                                          $tagname="全素";
                                             break;
                                             case 3:
                                          $tagname="蛋奶素";
                                             break;
                                             case 4:
                                          $tagname="無麩質";
                                             break;
                                             case 5:
                                          $tagname="加工品";
                                             break;
                                             case 6:
                                          $tagname="限時";
                                             break;
                                             case 7:
                                          $tagname="限量";
                                             break;
                                             case 8:
                                          $tagname="NG品";
                                             break;
                                             case 9:
                                          $tagname="獲獎";
                                             break;
                                        }
                                         echo  "<div class=\"tagstyle px-2\">$tagname</div>";
                                       } ?>
                                      </div>
                                </div>
                           </form>
                       </div>
                   </div>
               </div>


            </div>
        </div>
    </div>

                     </tr> 
     
                      
                     <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

  </div>

 

  
</div>






<script src="../assets/js/lightbox.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
//拖拉
$( function() {
    $( "#sortable" ).sortable({
      handle: 'label',
      axis: "y",
      placeholder: "ui-state-highlight",
      opacity: 0.8 ,
    });
    // $( "#sortable" ).disableSelection();
  } );

// lightbox
lightbox.option({
      'albumLabel': '第 %1 張 , 共 %2 張',
      'wrapAround': true
      })

// 選取的動畫 
let countR = $("tbody :checkbox").length
$("tbody :checkbox").click(function(){
        let checked = $(this).prop("checked")
        let checkedCount = $("tbody :checked").length; 
        if(checked){
            $(this).closest("tr").css("background","pink").css("animation","shake 2s infinite")
            $(".delete_all").addClass("red").css("animation","swing 2s infinite").css("font-size","40px")
            $(".outobj_all").addClass("text-danger").css("animation","swing 2s infinite").css("font-size","40px")    
            $(".onobj_all").addClass("text-success").css("animation","swing 2s infinite").css("font-size","40px") 
        }else{
            $(this).closest("tr").css("background","none").css("animation","none")
            $(".delete_all").removeClass("red").css("animation","none").css("font-size","28px")
            $(".outobj_all").removeClass("text-danger").css("animation","none").css("font-size","28px")     
            $(".onobj_all").removeClass("text-success").css("animation","none").css("font-size","28px")  
        }
        if(checkedCount==countR && checkedCount>0){
                $("#checkAll").prop("checked",true)
                $(".delete_all").addClass("red").css("animation","swing 2s infinite").css("font-size","40px")
            $(".outobj_all").addClass("text-danger").css("animation","swing 2s infinite").css("font-size","40px")    
            $(".onobj_all").addClass("text-success").css("animation","swing 2s infinite").css("font-size","40px")
            }else{
                $("#checkAll").prop("checked",false)
                $(".delete_all").addClass("red").css("animation","swing 2s infinite").css("font-size","40px")  
            $(".outobj_all").addClass("text-danger").css("animation","swing 2s infinite").css("font-size","40px")      
            $(".onobj_all").addClass("text-success").css("animation","swing 2s infinite").css("font-size","40px")  
            }
            
})

$("#checkAll").click(function(){ 
            let checkeded = $("tbody :checkbox").prop("checked")
            console.log(checkeded)
            let checkedAll = $(this).prop("checked")
            $("tbody :checkbox").prop("checked",checkedAll)
            if(!checkeded){
            $("tbody :checkbox").closest("tr").css("background","pink").css("animation","shake 2s infinite")
            $(".delete_all").addClass("red").css("animation","swing 2s infinite").css("font-size","40px")
            $(".outobj_all").addClass("text-danger").css("animation","swing 2s infinite").css("font-size","40px")    
            $(".onobj_all").addClass("text-success").css("animation","swing 2s infinite").css("font-size","40px")    
        }else{
            $("tbody :checkbox").closest("tr").css("background","none").css("animation","none")
            $(".delete_all").removeClass("red").css("animation","none").css("font-size","28px") 
            $(".outobj_all").removeClass("text-danger").css("animation","none").css("font-size","28px")      
            $(".onobj_all").removeClass("text-success").css("animation","none").css("font-size","28px")    
        }
        })

let checkAll = $('#checkAll'); //控制所有勾選的欄位
let checkBoxes = $('tbody .checkboxeach input'); //其他勾選欄位


checkAll.click(function() {
            for (let i = 0; i < checkBoxes.length; i++) {
                checkBoxes[i].checked = this.checked;
            }
        })


        function delete_all() {
            let sids = [];
            checkBoxes.each(function() {
                if ($(this).prop('checked')) {
                    sids.push($(this).val())
                }
            });
            if (!sids.length) {
                alert('沒有選擇任何資料');
            } else 
            {
               Notiflix.Confirm.Show(
                 '! 提醒 !',
                 '確定要刪除這些資料嗎?',
                 '確認',
                 '返回',
             function() {
               location.href = 'farmer_product_delete_all.php?sids=' + sids.toString();
             }
            );   
           
            }               
            //       {
            //     if(confirm('確定要刪除這些資料嗎？')){
            //         location.href = 'farmer_product_delete_all.php?sids=' + sids.toString();
            //     }

            // }
        }

        function onobj_all() {
            let sids = [];
            checkBoxes.each(function() {
                if ($(this).prop('checked')) {
                    sids.push($(this).val())
                }
            });
            if (!sids.length) {
                alert('沒有選擇任何資料');
            } else 
            {
               Notiflix.Confirm.Show(
                 '! 提醒 !',
                 '確定要上架這些資料嗎?',
                 '確認',
                 '返回',
             function() {
               location.href = 'farmer_product_onobj_all.api.php?sids=' + sids.toString();
             }
            );   
           
            }               
        }

        function outobj_all() {
            let sids = [];
            checkBoxes.each(function() {
                if ($(this).prop('checked')) {
                    sids.push($(this).val())
                }
            });
            if (!sids.length) {
                alert('沒有選擇任何資料');
            } else 
            {
               Notiflix.Confirm.Show(
                 '! 提醒 !',
                 '確定要下架這些資料嗎?',
                 '確認',
                 '返回',
             function() {
               location.href = 'farmer_product_outobj_all.api.php?sids=' + sids.toString();
             }
            );   
           
            }               
        }     


       
            

  
//修改 刪除fn
  function edit_one(sid) {
           Notiflix.Confirm.Show(
        '! 提醒 !',
        '確定要進行修改嗎?',
        '確認',
        '返回',
        function() {
          location.href = 'farmer_product_edit.php?sid=' + sid;
        }
        );   
           
        }               

//確認改顏色
    Notiflix.Confirm.Init({
    width: "300px",
    okButtonBackground: "#ce4e4e",
    titleColor: "#e81616",
    titleFontSize: "20px",
    fontFamily: "Arial",
    useGoogleFont: false,
      });     
  function delete_one(sid) {
          Notiflix.Confirm.Show(
        '! 提醒 !',
        '確定要進行刪除嗎?',
        '確認刪除',
        '返回',
        function() {
          location.href = 'farmer_product_delete.php?sid=' + sid;
        }
        );   
           
        }      

//搜尋
        function search() {
        document.location.href=`farmer_product_read.php?search_value=${search_value.value}`;

    }
    

        
  </script>



<?php include '../__html_foot.php' ?>