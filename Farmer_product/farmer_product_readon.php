<?php require '../__admin_required.php'?>
<?php require '../__connect_db.php' ?>
<?php
$page_name = "farmer_product_readon";
$page_title ='上架商品清單';

//取GET值

$value = isset($_GET['search_value']) ? $_GET['search_value']: '';
$page = isset($_GET['page']) ? intval($_GET['page']) : 1;

$per_page = 10; // 每一頁要顯示幾筆

//搜尋 設值取輸入值
$where = " WHERE 1 ";
if (!empty($value)) {
    $value1 = $pdo->quote("%$value%");
    $where .= " AND (`name` LIKE $value1 OR `price` LIKE $value1 OR `writing` LIKE $value1 OR `content` LIKE $value1) ";
}

// 已限制`shelves` = 1
$t_sql = "SELECT COUNT(1) FROM `farmer_product` $where";
$t_stmt = $pdo->query($t_sql);


$totalRows = $t_stmt->fetch(PDO::FETCH_NUM)[0]; // 拿到總筆數

$notice="查無搜尋結果";


if($totalRows==0){
    header('Location:farmer_product_readon.php?notice=' . "查無搜尋結果" );
    
    exit;
}

// $t_sql = "SELECT COUNT(1) FROM `farmer_product`";

// $t_stmt = $pdo->query($t_sql);
// $totalRows = $t_stmt->fetch(PDO::FETCH_NUM)[0]; // 拿到總筆數
//$totalRows = $pdo->query($t_sql)->fetch(PDO::FETCH_NUM)[0]; // 拿到總筆數

$totalPages = ceil($totalRows/$per_page); // 取得總頁數

if($page<1){
  header('Location: farmer_product_readon.php?page=' . 1 .'&search_value=' . $value  );
  exit;  //直接離開不在跑下面
}
if($page>$totalPages){
    header('Location: farmer_product_readon.php?page='. $totalPages. '&search_value=' . $value);
    exit;
  }

//farmer_product表拿取限制上架中`shelves` = 1

if($value =='' ){
        
  $sql = sprintf(
      "SELECT * FROM `farmer_product` LIMIT %s, %s",
      ($page - 1) * $per_page,
      $per_page
  );
  $stmt = $pdo->query($sql); 
}else{      
    $sql = "SELECT * FROM `farmer_product` $where ORDER BY `name` DESC LIMIT " . ($page - 1) * $per_page . "," . $per_page;
    $stmt = $pdo->query($sql);
}




?>
<?php include '../__html_head.php' ?>
<?php include '../__html_body.php' ?>

<style>
.box_pic{
  width:150px;
  height:120px;
  overflow: hidden;
  margin:0 5px;
  border-radius:5px;
}
.pic{
  width: 100%;
  height:100%;  
  object-fit: cover;
  object-position: center;
}
</style>

<!-- div -->
<div class="content mt-0 mb-2" >

<!-- 包按鈕和搜尋 -->
      <div class="mb-2 d-flex justify-content-between row col-md-12">

<!-- 頁數按鈕 -->
<nav aria-label="Page navigation example " >
  <ul class="pagination justify-content-center">
  <li class="page-item">
    <a class="page-link" href="?page=1">
    <i class="fas fa-angle-double-left"></i></a></li>
  <li class="page-item">
    <a class="page-link" href="?page=<?=$page-1?>">
    <i class="fas fa-chevron-left"></i></a></li>
  <?php 
  $p_start=$page-3;
  $p_end=$page+3;
  for($i=$p_start; $i<=$p_end;$i++): 
  if($i<1 or $i>$totalPages)continue;
  ?>
    <li class="page-item <?= $i==$page? 'active' : '' ?>">
    <a class="page-link" href="?page=<?= $i ?>"><?= $i?></a></li>
<?php endfor; ?>
    <li class="page-item">
    <a class="page-link" href="?page=<?=$page+1?>">
    <i class="fas fa-chevron-right"></i></a></li>
    <li class="page-item">
    <a class="page-link" href="?page=<?=$totalPages?>">
    <i class="fas fa-angle-double-right"></i></a></li>
  </ul>
</nav>
<!-- 包按鈕和輸入 -->
            <div class="row align-items-center">
<!-- 搜尋BAR -->            
               <input type="text" class="form-control" id="search_value" name="search_value"
                placeholder="<?=isset($_GET['notice'])?$_GET['notice']: ''?>" style="margin:0 10px; transition: 0.5s; width:150px;z-index:100;">
               <a id="search_search" style="line-height:50% transition: 0.5s;" href="javascript:search()"><i class="fas fa-search fa-2x mt-1"></i></a>     
            </div>
         

    </div>

<!-- 展示列表 -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header row d-flex justify-content-between align-items-center">
                <div class="row"> <h4 class="card-title ml-5">商品資訊</h4>
                 <a href="farmer_product_insert.php" class="ml-1">
                        <i class="fas fa-plus"></i>
                 </a>
                </div>

                <div class="ml-5 ">查詢資料總筆數:<?=$totalRows?> </div>
               
                <div class="mr-5 row">
                
                <a href="javascript:delete_all()"><div class="btn btn-info"> <i class="fas fa-trash-alt delete_all"></i> 選取刪除 </div></a>
                <a href="javascript:outobj_all()"><div class="btn btn-info"> 下架 </div></a>
                <a href="javascript:obj_less()"><div class="btn btn-info"> 庫存緊張 </div></a>
                <button class="btn btn-info">缺貨中</button>
                </div>
                
              </div>
              
              
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                      <th scope="col" style="vertical-align:left;">
                        <label class='checkbox-inline checkboxeach'>
                            <input id='checkAll' type='checkbox' name='checkboxall' value='1'></label>全選
                            
                      </th>
                      <!-- <th><input type="checkbox">全選</th> -->
                      <th>商品名</th>
                      <th>圖片</th>
                      <th>價格</th>
                      <th>庫存</th>
                      <th>上傳日期</th>
                      <th>狀態</th>
                      <th>顯示更多</th>
                      <th>編輯</th>
                      <th>刪除</th>
                      
                    </thead>
                    <tbody>
                    <tr>
                      
                    <?php while($r=$stmt->fetch()){  ?>
                      <td> <label class=' checkbox-inline checkboxeach'>
                                <input id="<?= 'readtrue' . $r['sid'] ?>" type='checkbox' name=<?= 'readtrue' . $r['sid'] . '[]' ?> value='<?= $r['sid'] ?>'> <!-- 選取框 -->
                      </label> </td>
                   <!-- <td> <input type="checkbox" ></td> -->
                   <td><?= $r['name'] ?></td>
                   <td class="d-flex"> 
                     <?php
                  //  $r['picture']
                    $pic=json_decode($r['picture'], JSON_UNESCAPED_UNICODE);
                    if(isset($pic)){
                       
                      foreach($pic as $phot){
                        // echo " <img src=uploads/"."$phot" . " , "."\""."alt=\"\" width=\"150\">" ;
                        echo "<a href=\"uploads/"."$phot"."\""."data-lightbox=\"ex1\"><div class=\"box_pic\"><img src=uploads/"."$phot" . " , "."\""."alt=\"\" class=\"pic\" ></div></a>" ;
                      }
                    } 
                    
                    ?></td>
                   <td><?= $r['price'] ?></td>
                   <?php if($r['stock']<=10){$co_stock="text-danger font-weight-bold";}else{$co_stock="text-dark";} ?>
                   <td class="<?=$co_stock?>"><?= $r['stock'] ?></td>
                   <td><?= $r['created_at'] ?></td>
                   <?php if($r['shelves']==0){
                     $onout="下架中" ;$color_onout="text-danger";}
                     else{$onout="上架中" ;$color_onout="text-success";} ?>
                   <td class="<?=$color_onout?>"><?= $onout ?></td>
                   <td>    
                      <a href=""><i class="fas fa-ellipsis-h"></i></a>
                  </td>
                   <td>
                     <a href="javascript:edit_one(<?= $r['sid'] ?>)"><i class="fas fa-pen"></i></a>&nbsp;&nbsp;
                       
                   </td>
                   <td>
                     <a href="javascript:delete_one(<?= $r['sid'] ?>)"><i class="fas fa-trash-alt "></i></a>
                   </td>
                     </tr> 
                    
                     <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

  </div>

  
</div>


<!-- 光箱 -->
<script src="../assets/js/lightbox.js"></script>

<script>

let checkAll = $('#checkAll'); //控制所有勾選的欄位
let checkBoxes = $('tbody .checkboxeach input'); //其他勾選欄位


checkAll.click(function() {
            for (let i = 0; i < checkBoxes.length; i++) {
                checkBoxes[i].checked = this.checked;
            }
        })

// 刪除全部
        function delete_all() {
            let sids = [];
            checkBoxes.each(function() {
                if ($(this).prop('checked')) {
                    sids.push($(this).val())
                }
            });
            if (!sids.length) {
                alert('沒有選擇任何資料');
            } else 
            {
               Notiflix.Confirm.Show(
                 '! 提醒 !',
                 '確定要刪除這些資料嗎?',
                 '確認',
                 '返回',
             function() {
               location.href = 'farmer_product_delete_all.php?sids=' + sids.toString();
             }
            );   
           
            }               
       
        }

// 下架
        function outobj_all() {
            let sids = [];
            checkBoxes.each(function() {
                if ($(this).prop('checked')) {
                    sids.push($(this).val())
                }
            });
            if (!sids.length) {
                alert('沒有選擇任何資料');
            } else 
            {
               Notiflix.Confirm.Show(
                 '! 提醒 !',
                 '確定要下架這些資料嗎?',
                 '確認',
                 '返回',
             function() {
               location.href = 'farmer_product_outobj_all.api.php?sids=' + sids.toString();
             }
            );   
           
            }               
        }

// 庫存緊張
function obj_less(){

 document.location.href=`farmer_product_readon.php?stock=${stock.value}`; 
 document.location.href=`farmer_product_readon.php?search_value=${search_value.value}`; 

}

  
//修改 
  function edit_one(sid) {
           Notiflix.Confirm.Show(
        '! 提醒 !',
        '確定要進行修改嗎?',
        '確認',
        '返回',
        function() {
          location.href = 'farmer_product_edit.php?sid=' + sid;
        }
        );   
           
        }               

  //確認改顏色
    Notiflix.Confirm.Init({
    width: "300px",
    okButtonBackground: "#ce4e4e",
    titleColor: "#e81616",
    titleFontSize: "20px",
    fontFamily: "Arial",
    useGoogleFont: false,
      });    

// 刪除fn
  function delete_one(sid) {
          Notiflix.Confirm.Show(
        '! 提醒 !',
        '確定要進行刪除嗎?',
        '確認刪除',
        '返回',
        function() {
          location.href = 'farmer_product_delete.php?sid=' + sid;
        }
        );   
           
        }      

//搜尋
        function search() {
        document.location.href=`farmer_product_readon.php?search_value=${search_value.value}`;

    }
    

        
  </script>
          


<?php include '../__html_foot.php' ?>