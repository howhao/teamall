<?php
require '../__admin_required.php';
require '../__connect_db.php';
$page_name = 'adminhome';
$page_title = 'AdminHome';

$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
$per_page = 10;//每頁幾筆
$t_sql = "SELECT COUNT(1) FROM `farmers` WHERE `status`=1";//資料幾筆
$t_stmt = $pdo->query($t_sql);
$totalRows = $t_stmt->fetch(PDO::FETCH_NUM)[0];//設為索引陣列,索引0為總筆數
$totalPages = ceil($totalRows / $per_page);//總頁數

//if ($page < 1) {
//    header('Location: farmer.php');
//    exit;
//}
//if ($page > $totalPages) {
//    header('Location: farmer.php?page=' . $totalPages);
//    exit;
//}
$result = [
    'page' => $page,
    'per_page' => $per_page,
    'totalRows' => $totalRows,
    'totalPages' => $totalPages,
    'rows' => [],
];

$sql = sprintf("SELECT * FROM `farmers` WHERE `status`=1 ORDER BY `farmer_id` ASC LIMIT %s, %s",
    ($page - 1) * $per_page,
    $per_page
);//(從第幾筆開始),(選擇幾筆)
$stmt = $pdo->query($sql);
$rows = $stmt->fetchAll();

?>
<?php include '../__html_head.php' ?>
<?php include '../__html_body.php' ?>

    <style>

        .bigbox {
            position: relative;
            width: 1500px;
            /* ADJUST WIDTH TO MATCH WIDTH OF YOUR TEXT */
            height: 1000px;
            /* ADJUST HEIGHT TO MATCH HEIGHT OF YOUR TEXT */
            margin: 40vh auto 0 auto;
        }

        #text01 {
            position: relative;
            width: 1500px;
            /* ADJUST WIDTH TO MATCH WIDTH OF YOUR TEXT */
            height: 1000px;
            /* ADJUST HEIGHT TO MATCH HEIGHT OF YOUR TEXT */

        }

        .title {
            stroke-dasharray: 3000;
            stroke-dashoffset: 3000;
            animation: draw 7s linear forwards;
            -webkit-animation: draw 7s linear forwards;
            -moz-animation: draw 7s linear forwards;
            -o-animation: draw 7s linear forwards;
            font-weight: bold;
            font-family: Amatic SC;
            -inkscape-font-specification: Amatic SC Bold "
        }

        .title1 {
            stroke-dasharray: 1000;
            stroke-dashoffset: 1000;
            animation: draw1 7s linear forwards;
            -webkit-animation: draw1 7s linear forwards;
            -moz-animation: draw1 7s linear forwards;
            -o-animation: draw1 7s linear forwards;
            font-weight: bold;
            font-family: Amatic SC;
            -inkscape-font-specification: Amatic SC Bold "
        }

        @keyframes draw {
            to {
                stroke-dashoffset: 0;
                fill-opacity: 1;
            }
        }

        @keyframes draw1 {
            to {
                stroke-dashoffset: 0;
                fill-opacity: 1;
            }
        }

        @-webkit-keyframes draw {
            to {
                stroke-dashoffset: 0;

            }
        }

        @-moz-keyframes draw {
            to {
                stroke-dashoffset: 0;
            }
        }

        @-o-keyframes draw {
            to {
                stroke-dashoffset: 0;

            }
        }
        /*#verify_confirm ,#verify_deny{*/
        /*    transition: .2s;*/
        /*}*/
        #verify_confirm:hover{
            color: #3087ff
        }
        #verify_deny:hover{
            color: #ff3f41
        }
    </style>
<?php if (!empty($rows)): ?>
    <div class="content ">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header row">
                        <h4 class="card-title ml-4">審核</h4>
                    </div>
                    <div class="card-body pt-1">
                        <div class="">
                            <table class="table">
                                <thead class=" text-primary">
                                <tr>
                                    <th><a id="id_desc" href="javascript: sort(0)">ID<i id="icon_desc" class="fas fa-caret-up"></i></a>
                                    </th>
                                    <th>Company</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Password</th>
                                    <th>Mobile</th>
                                    <th class="text-center">Approved</th>
                                    <th class="text-center">Deny</th>
                                </tr>
                                </thead>
                                <tbody id="t_content">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <nav aria-label="Page navigation" id="test1">
            <ul class="pagination justify-content-center">
            </ul>
        </nav>
    </div>
<?php else: ?>
    <svg id="圖層_1" data-name="圖層 1" xmlns="http://www.w3.org/2000/svg" viewBox="-550 -200 1500 1000">
        <defs>
            <style>.cls-1 {
                    fill: #59ba0d;
                }

                .cls-2 {
                    fill: #ffce00;
                }

               </style>
        </defs>
        <title>未命名-4</title>
        <path class="title cls-1" fill-opacity="0" stroke="#59ba0d" stroke-width="3.5"
              d="M181.08,316.14c0,.22,0,.44,0,.65.36,11.77,10.4,21.21,22.73,21.21H206a132.93,132.93,0,1,0-100.82-30.29s16.43,18.05,32.17,14.55,14-15.74,14-19.24-16.28-13.34-16.28-13.34a102.59,102.59,0,0,1-40.56-82c0-56.51,45.22-102.31,101-102.31s101,45.8,101,102.31c0,47.87-31.13,97.46-76.31,99.23-10.7.41-10.88-8.29-10.88-8.29v-35s24.17-10.49,24.17-31.48V162.24c0-4.83-4.25-8.75-9.49-8.75s-9.5,3.92-9.5,8.75v49H202.42v-49c0-4.83-4.25-8.75-9.5-8.75s-9.49,3.92-9.49,8.75v49H171.34v-49a8.63,8.63,0,1,0-17.26,0v68.2s3.45,19.24,25.89,33.23c0,7.65.46,33.52.71,47l.4,6.37"/>
        <path class="title cls-1" fill-opacity="0" stroke="#59ba0d" stroke-width="3.5"
              d="M304.38,1.34s-50.72,35-8.75,99.69C302.63,95.78,351.6,48.56,304.38,1.34Z"/>
        <path class="title cls-1" fill-opacity="0" stroke="#59ba0d" stroke-width="3.5"
              d="M403.28,115.8s-35-50.72-99.69-8.74C308.84,114.05,356.06,163,403.28,115.8Z"/>
        <path class="title1 cls-2" fill-opacity="0" stroke="#ffce00" stroke-width="1.5"
              d="M16.88,145.27l.9-3.69.18-.74c.52-2.14,1.7-3,3.53-2.54a3.9,3.9,0,0,1,2.58,2,4.74,4.74,0,0,1,.41,3.51l-.19.74L23,149.71l12.35,8.89q12.78,9.24,10,20.56A13.8,13.8,0,0,1,39.2,188a13.32,13.32,0,0,1-10.57,1.55,18,18,0,0,1-8.08-4.41A22.49,22.49,0,0,1,15,177a29.75,29.75,0,0,1-1.95-9.16,31.53,31.53,0,0,1,.79-9.58l.86-4.06.28-1.26L2.54,144.08l-.73-.51a4.52,4.52,0,0,1-1.95-2.25,7.31,7.31,0,0,1,.25-3.56l3.57-14.67c.65-2.65,1.9-3.75,3.77-3.3a3.82,3.82,0,0,1,2.53,2,4.74,4.74,0,0,1,.4,3.5l-.18.77-3,12.21Zm4.29,12.11-.37,1.54q-2.07,8.47.56,14.47,2.94,6.72,8.37,8a7.2,7.2,0,0,0,5.33-.59,5.92,5.92,0,0,0,3.06-4q1.5-6.18-7.34-12.54Z"/>
        <path class="title1 cls-2" fill-opacity="0" stroke="#ffce00" stroke-width="1.5"
              d="M21.07,111.72a4.06,4.06,0,0,1,5.47-1.87A4.09,4.09,0,1,1,23,117.2a4.08,4.08,0,0,1-1.88-5.48Zm21.2,14.69,1.46-3c1.08-2.23,2.41-3,4-2.19a3.33,3.33,0,0,1,1.94,2.41,6,6,0,0,1-.71,3.73l-2.06,4.21a6.9,6.9,0,0,1-2.74,3.19,3.35,3.35,0,0,1-3.25.07,6.8,6.8,0,0,1-2-1.64l-8.6-9.61-.6-.66c-1.6-1.78-1.95-3.57-1.07-5.37A3.07,3.07,0,0,1,30.34,116a2.64,2.64,0,0,1,2.17.06,8.73,8.73,0,0,1,2.24,2Z"/>
        <path class="title cls-2" fill-opacity="0" stroke="#ffce00" stroke-width="1.5"
              d="M44.12,104.15l1.48,2.22c1.31,2,1.55,3.58.74,4.78a3.14,3.14,0,0,1-1.9,1.37,2.52,2.52,0,0,1-2.13-.38,7.41,7.41,0,0,1-1.53-1.82l-4.92-7.38q-1.77-2.67-.53-4.5a2.48,2.48,0,0,1,3.21-1l.89.32a1.26,1.26,0,0,0,1,.06,5,5,0,0,0,1.12-1.34l3.88-5.69c1.45-2.12,2.92-2.68,4.41-1.66q3.09,2.1.34,6.14l.06.44c.71,4.26,1.73,6.84,3.08,7.76s2.53.63,3.54-.86L59,99.49q2.1-3.09,4.25-1.63a3.38,3.38,0,0,1,1.6,2.68,5.94,5.94,0,0,1-1.24,3.59l-3.25,4.77A6.24,6.24,0,0,1,56,111.73a7.5,7.5,0,0,1-5.41-1.42,8.69,8.69,0,0,1-2.74-3.08,24,24,0,0,1-1.95-5.61Z"/>
        <path class="title1 cls-2" fill-opacity="0" stroke="#ffce00" stroke-width="1.5"
              d="M65.57,79.92l.61,1.33q1.23,2.94-.4,4.68a3.15,3.15,0,0,1-2.09,1,2.48,2.48,0,0,1-2.06-.62,6.6,6.6,0,0,1-1.41-2.5L57.1,75.52c-.73-2-.6-3.53.38-4.57a2.84,2.84,0,0,1,1.9-.93,2.32,2.32,0,0,1,1.86.62,7,7,0,0,1,.71.92,7.86,7.86,0,0,0,.76.87A6.64,6.64,0,0,0,66.43,74l2.84.37a11.88,11.88,0,0,1,6.78,2.84,8,8,0,0,1,2.62,6.26,9.93,9.93,0,0,1-2.9,6.73L71.56,94.7q-2.57,2.73-4.45,1A3.36,3.36,0,0,1,66,92.77a5.81,5.81,0,0,1,1.76-3.36l2.33-2.49q2.81-3,.64-5a5.73,5.73,0,0,0-2.39-1.19Z"/>
        <path class="title1 cls-2" fill-opacity="0" stroke="#ffce00" stroke-width="1.5"
              d="M83.81,53.08l.71-.57q2.91-2.35,4.56-.31a3.31,3.31,0,0,1,.72,3,5.9,5.9,0,0,1-2.22,3.07l-1.67,1.35,3,10,2.56-2.07c2-1.56,3.46-1.66,4.56-.3a3.29,3.29,0,0,1,.73,3,5.93,5.93,0,0,1-2.23,3.08l-4.1,3.3A5.6,5.6,0,0,1,87,78.08a3.35,3.35,0,0,1-2.77-1.36,6.44,6.44,0,0,1-1.11-2.21l-.37-1.36L80.1,64.3l-.34.27q-2.57,2.07-4.17.08a3.4,3.4,0,0,1-.77-3A5.94,5.94,0,0,1,77,58.52l1.06-.85-1.2-4-.25-.82q-1-3.42,1.34-5.28a3,3,0,0,1,2.13-.67,2.57,2.57,0,0,1,1.9,1,7.33,7.33,0,0,1,1.18,2.68Z"/>
        <path class="title1 cls-2" fill-opacity="0" stroke="#ffce00" stroke-width="1.5"
              d="M144.79,23.76l3.57-1.28.72-.26c2.07-.74,3.43-.22,4.06,1.55a3.91,3.91,0,0,1-.25,3.27,4.75,4.75,0,0,1-2.71,2.26l-.72.26-5,1.79-.62,15.2q-.67,15.75-11.65,19.68a13.85,13.85,0,0,1-10.83-.29,13.3,13.3,0,0,1-7.11-8,18,18,0,0,1-.77-9.17,22.39,22.39,0,0,1,3.79-9.11A30,30,0,0,1,123.85,33a31.71,31.71,0,0,1,8.43-4.61l3.87-1.52,1.21-.46.53-15.29,0-.89a4.47,4.47,0,0,1,.81-2.86,7.4,7.4,0,0,1,3.11-1.76L156,.53q3.86-1.38,4.83,1.34a3.8,3.8,0,0,1-.29,3.21,4.7,4.7,0,0,1-2.69,2.26l-.75.27-11.83,4.24ZM137,34l-1.49.53q-8.2,2.94-11.77,8.44-4,6.15-2.11,11.42a7.29,7.29,0,0,0,3.43,4.12,6,6,0,0,0,5,.37q6-2.14,6.43-13Z"/>
        <path class="title1 cls-2" fill-opacity="0" stroke="#ffce00" stroke-width="1.5"
              d="M178.09,17.55a9.57,9.57,0,0,1,7.27,2,9.23,9.23,0,0,1,3.46,6.56,14.57,14.57,0,0,1-.77,6.37,13.55,13.55,0,0,1-3.2,5.33,8.87,8.87,0,0,1-3,2,17.88,17.88,0,0,1-4.5.93q-4.77.51-6.56-.45a9.08,9.08,0,0,1-4.77-7.55,13.34,13.34,0,0,1,3-10.06A13,13,0,0,1,178.09,17.55Zm0,6.31a5.2,5.2,0,0,0-3.73,2.56,7.28,7.28,0,0,0-1.28,4.9q.36,3.41,4.17,3a4.36,4.36,0,0,0,3.28-1.74,7.68,7.68,0,0,0,1.44-5.37,3.84,3.84,0,0,0-1.25-2.59A3.36,3.36,0,0,0,178.05,23.86Z"/>
        <path class="title1 cls-2" fill-opacity="0" stroke="#ffce00" stroke-width="1.5"
              d="M209.7,17.54a9.56,9.56,0,0,1,6.83,3.15,9.28,9.28,0,0,1,2.32,7A14.89,14.89,0,0,1,217,33.89a13.62,13.62,0,0,1-4,4.71,9,9,0,0,1-3.27,1.46,17.77,17.77,0,0,1-4.6.17q-4.78-.3-6.39-1.56a9.08,9.08,0,0,1-3.43-8.24A13.33,13.33,0,0,1,199.92,21,13,13,0,0,1,209.7,17.54Zm-1.1,6.21a5.17,5.17,0,0,0-4.11,1.9,7.23,7.23,0,0,0-2.09,4.61c-.14,2.28,1.06,3.49,3.61,3.65a4.29,4.29,0,0,0,3.52-1.16,7.61,7.61,0,0,0,2.33-5,3.82,3.82,0,0,0-.8-2.77A3.38,3.38,0,0,0,208.6,23.75Z"/>
        <path class="title1 cls-2" fill-opacity="0" stroke="#ffce00" stroke-width="1.5"
              d="M243.41,40.55l3.26.81q3.63.9,3,3.44a3.35,3.35,0,0,1-2,2.4,6,6,0,0,1-3.79.09l-3.85-1a3.59,3.59,0,0,1-2.81-2.64,5.66,5.66,0,0,1-4.37.86l-3.62-.91a6.57,6.57,0,0,1-4.54-3.43,8.72,8.72,0,0,1-.46-6.24,15.3,15.3,0,0,1,6.22-9q4.84-3.46,9.54-2.3a7,7,0,0,1,4.89,3.85l10.26-14c1.75-2.4,3.59-3.35,5.53-2.87a3.11,3.11,0,0,1,1.9,1.3,2.59,2.59,0,0,1,.37,2.14,5.68,5.68,0,0,1-.92,1.9l-.54.68Zm-10.49-2.46,1.09.27a2.59,2.59,0,0,0,3-.92l3.45-4.76a4.31,4.31,0,0,0,.77-1.38,1.88,1.88,0,0,0-.36-1.64,2.85,2.85,0,0,0-1.67-1.05q-2.22-.55-4.77,1.45A8.88,8.88,0,0,0,231.15,35C230.73,36.65,231.32,37.69,232.92,38.09Z"/>
    </svg>
<?php endif; ?>

    </div></div>
    <script>
        const bell_active = $('#navbarDropdownMenuLink');
        const bell_notice = $('#bell_notice');
        const pagination = document.querySelector('.pagination');
        const t_content = document.querySelector('#t_content');
        const id_desc = document.querySelector('#id_desc');
        const sort_ud = [
            'ID<i id="icon_desc" class="fas fa-caret-up"></i>',
            'ID<i id="icon_desc" class="fas fa-caret-down"></i>'
        ];
        const pagination_str = `
            <li class="page-item <%= active %>">
                <a class="page-link" href="javascript:loadData(<%= i %>)"><%= i %></a>
            </li>
        `;
        const table_row_str = `
        <tr>
                <td><%= farmer_id %></td>
                <td><%= company %></td>
                <td><%= name %></td>
                <td><%= email %></td>
                <td><%= password %></td>
                <td><%= mobile %></td>
                <td class="text-center"><a id="verify_confirm" href="javascript: verify(<%= farmer_id %>,2)"><i class="fas fa-check fa-lg"></i></i></a></td>
                <td class="text-center"><a id="verify_deny" href="javascript: verify(<%= farmer_id %>,0)"><i class="fas fa-ban fa-lg"></i></a></td>
            </tr>
        `;

        const pagination_fn = _.template(pagination_str);

        const table_row_fn = _.template(table_row_str);
        function verify(id,status){
            fetch('adminHome_verify_api.php?id='+ id +'&status=' + status)
                .then(response => {
                    console.log(response);
                    return response.json();
                })
                .then(json => {
                    console.log(json);
                    if(json.success){
                        let page = pagination_active;
                        loadData(page);
                    }
                })
        }

        var sort_v = 0 ;

        function sort(s){
            let page = pagination_active;

            let sort;
            if(id_desc.innerHTML==sort_ud[0]){
                sort = 0;
                sort_v = 1;
                id_desc.innerHTML=sort_ud[1];
                id_desc.href = "javascript: sort(1)";
            }else if(id_desc.innerHTML==sort_ud[1]){
                sort = 1;
                sort_v = 0;
                id_desc.innerHTML=sort_ud[0];
                id_desc.href = "javascript: sort(0)";
            }
            loadData(page);
        }

        function loadData(page = 1){
            fetch('adminHome_api.php?page='+ page + '&sort=' + sort_v)
                .then(response => {
                    // console.log(response);
                    return response.json();
                })
                .then(json => {
                    console.log(json.sort);
                    let i, s, item;
                    let t_str = '';
                    for (s in json.rows) {
                        item = json.rows[s];
                        t_str += table_row_fn(item);
                    }
                    // console.log(item);
                    t_content.innerHTML = t_str;

                    let p_str = '';
                    for (i = 1; i <= json.totalPages; i++) {
                        let active = i === json.page ? 'active' : '';
                        p_str += pagination_fn({
                            i: i,
                            active: active
                        });
                    }
                    pagination.innerHTML = p_str;
                    pagination_active = document.querySelector('ul.pagination li.active a').innerHTML;

                });

        }
        loadData();

    </script>
<?php include '../__html_foot.php' ?>