<?php

require '../__connect_db.php';
$page_name = 'dinner_list';
$page_title = 'dinner_list';

// 測試餐廳
$restaurant_id = 81;


// 拿副食材名稱
$sql = "SELECT `class_sid`, `name` FROM `product_class` WHERE `category_sid`=7";

$stmt = $pdo->query($sql);
$rows = $stmt->fetchAll(PDO::FETCH_NUM);

$rows_other_ingred=[];
foreach ($rows as $key => $value) {
  $rows_other_ingred += [$value[0] => $value[1]];
}

$sql = "SELECT `class_sid`, `name` FROM `product_class` WHERE `category_sid`=10";

$stmt = $pdo->query($sql);
$rows = $stmt->fetchAll(PDO::FETCH_NUM);

foreach ($rows as $key => $value) {
  $rows_other_ingred += [$value[0] => $value[1]];
}

$sql = "SELECT `class_sid`, `name` FROM `product_class` WHERE `category_sid`=11";

$stmt = $pdo->query($sql);
$rows = $stmt->fetchAll(PDO::FETCH_NUM);

foreach ($rows as $key => $value) {
  $rows_other_ingred += [$value[0] => $value[1]];
}


?>
<?php include 'value_match.php' ?>
<?php include '../__html_head.php' ?>
<?php include '../__html_body.php'   ?>

<style>
  *{
    font-family: "Microsoft JhengHei";
  }
  /*測試5555*/ 
  .my_list{
    margin: auto;
  }

  .img_wrap {
    width: 150px;
    height: 150px;
    overflow: hidden;
    border-radius: 5%;
    margin: 0 5px;
  }

  .img_wrap img{
    width: 100%;
    height: 100%;
    object-fit: cover;
  }

  .dinner_img {
    width: 22vw;
    /* border: 1px solid #aaa; */
    display: flex;
    justify-content: center;
  }

  .table_row{
    display: flex;
  }

  .table_row td{
    /* border: 1px solid #aaa; */
    display: flex;
    align-items: center;
  }

  .food{
    flex-flow: column wrap;
    justify-content: center;
  }
  .box{
            position:relative;
            box-shadow: 0 1px 5px #666;
            background: #fff;
            width:20%;
          }
          .box.active{
            background: rgba(255,150,150,0.8);
          }
          .box_inside{
            padding:40px 20px 20px;
          }
          .radius_border{
            border-radius: 4px;
            overflow: hidden;
          }
          .banner{
            height:160px;
            background: #000;
            background: url() center center no-repeat;
            background-size: cover;
            position: relative;
          }
          .image_wrap{
            position: absolute;
            top: 110px;
            left: 200%;
            transform: translate(-50%, -50%);
            display: flex;
            width: 100%;
            justify-content: center;
            transition: .5s;
          }
          .image_wrap.active{
            left:50%;
          }

          figure{
            width:120px;
            height:120px;
            background: purple;
            color:#fff;
            transform: rotate(360deg);
            transition: .5s;
          }

          .active figure{
            transform: rotate(0deg);
          }

          figure img{
            width: 100%;
            height: 100%;
            object-fit: cover; 
          }
          .circle{
            border-radius: 50%;
            overflow: hidden;
          }
          h3{
            text-align: center;
            color:#333;
            padding-bottom: 10px;
            margin-bottom: 10px;
            border-bottom: 1px solid #aaa;
          }
          p{
            line-height: 1.7;
            color:#aaa;
          }
          p.active{
            color:#fff;
          }
          .intro{
            cursor: pointer;
            min-height: 50px;
            border: 1px solid #aaa;
            border-radius: 4px;
            display: block;
            padding:15px 10px;
            text-align: center;
            margin-top: 10px;
            background: #F9F8BD;
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff5db1', endColorstr='#ef017c',GradientType=0 ); /* IE6-9 */
          }
          .intro:hover{
            background: #F9F8BD;
          }
          .intro.active{
              min-height: 200px;
          }
          .transition{
              transition: .5s;
          }
          .ellipsis{
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
          }

</style>

<div class="container">
    <div>
        <pre><?php
            if(! empty($_FILES)){
               var_dump($_FILES);
            }
             
            if(! empty($_POST)){
              var_dump($_POST);
            }
          
          // print_r($rows_other_ingred);
              
        ?>
        </pre>
    </div>

<div class="my_list">
  <div class="row">
      <nav aria-label="Page navigation example">
        <ul class="pagination pagination-sm flex"></ul>
        <!-- <div class="test"></div> -->
      </nav>
  </div>
</div>

<div class="row align-items-center">
<button class="btn btn-primary show" data-show="card">Card</button>
<button class="btn btn-primary show" data-show="list">List</button>
<p class="" id="rowCount"></p>
</div>

<div>
  <div class="input-group mb-3 d-flex align-items-center">          
    <input type="text"  id="search" name="search" class="form-control col-md-4" placeholder="請輸入">
      <button class="btn btn-warning" type="button" id="button-addon2" onclick="loadData(Obj.searchName)">搜尋</button>


    <button class="btn btn-success" id="push" data-dom="上架">選取項目上架</button>
    <button class="btn btn-success" id="drop" data-dom="下架">選取項目下架</button>
  </div>
</div>

<div class="card my_list container-fluid">
  <div id="my_row" class="row">

</div>
</div>

<script>

  let vice_food =[];
  vice_food = <?= json_encode($rows_other_ingred, JSON_UNESCAPED_UNICODE); ?>;
  // console.log(vice_food[34])

  let toggle = true;
  let page = 0;
  let delete1 = `<a href="javascript:delete_conf(<%= dinner_id %>)"><i class="fas fa-trash-alt fa-2x"></i></a>`;

  let edit = '<a href="dinner_edit.php?dinner_sid=<%= dinner_id %>"><i class="fas fa-edit fa-2x"></i></a>';

  let checked = '<input type="checkbox" class="my_choose" name="my_choose[]" value="<%= dinner_id %>" disabled>';

  let change = false;
  let number = 0;

  let Obj = {
    page: 1,
    number: 0,
    sid: 0,
    searchName: '',
    active: '',
    searchType: '',
  }

  let push = document.querySelector('#push')
  

  let change_i = document.querySelector('#change');

  const pagination = document.querySelector('.pagination');

      const pagination_str = `
              <li class="page-item <%= active %>">
                  <a id="pageLink" class="page-link" href="" data-page="<%= i %>"><%= i %></a>
              </li>
          `;
      const big_table = `<div class="my_list">
                          <table class="table text-center">
                            <thead>
                              <tr class="t_head d-flex justify-content-around">
                                <th scope="col"><input type="checkbox" id="checkTotal" disabled>全選</th>
                                <th scope="col"><a href="javascript:changeSort()" id="change"><i class="fas fa-sort-up"></i></a></th>
                                <th scope="col">刪除</th>
                                <th scope="col">餐廳名稱</th>
                                <th scope="col">主食或主菜</th>
                                <th scope="col">菜色名稱</th>
                                <th scope="col">辣度</th>
                                <th scope="col">簡介</th>
                                <th scope="col">可更換主食材</th>
                                <th scope="col">副食材內容</th>
                                <th scope="col">食材商品名</th>
                                <th scope="col">總價</th></th>
                                <th scope="col">狀態</th>
                                <th scope="col" class="dinner_img">菜色照片</th>
                                <th scope="col">編輯</th>
                              </tr>
                            </thead>
                            <tbody id="t_content">

                            </tbody>
                          </table>
                          </div>`;

      const table_row_str = `
        <tr class="table_row justify-content-around">
            <td> ${checked} </td>
            <td><%= dinner_id %></td>
            <td> ${delete1} </td>
            <td><%= restaurant_id %></td>
            <td><%= small_cat %></td>
            <td><%= name %></td>
            <td><%= spicy %></td>

            <td>
              <!-- Button trigger modal -->
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#intro-<%= dinner_id %>">
                詳細內容
              </button>

              <!-- Modal -->
              <div class="modal fade" id="intro-<%= dinner_id %>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <%= intro %>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">收回</button>
                    </div>
                  </div>
                </div>
              </div>              
            </td>     

            <td class="d-flex food"><%
            _.forEach(food, function(food) { %><div><%- food %></div><% });
            %></td>

            <td class="d-flex food"><%
            _.forEach(other_ingred, function(other_ingred) { %><div><%- other_ingred %></div><% });
            %></td>

            <td class="d-flex food"><%
            _.forEach(product, function(product) { %><div><%- product %></div><% });
            %></td>
            <td><%= Number(money) + Number(price) %></td>
          
            <td><%= onboard %></td>

            <td class="dinner_img"><%
            _.forEach(dinner_image, function(dinner_image) { %><div class="img_wrap"><a href="my_images/<%- dinner_image %>" data-lightbox="image-<%= dinner_id %>" data-title="My caption"><img src="my_images/<%- dinner_image %>"></img></div><% });
            %></a></td>
            <td> ${edit} </td>
        </tr>
       `;

  const card_content = document.querySelector('#my_row');
 
  const test_str = 
            `<div class="box animated radius_border col-md-4" id="<%= dinner_id %>">
                <div class="banner"></div>

                <div class="image_wrap">
                  <%_.forEach(dinner_image, function(dinner_image) { %> <figure class="circle"><a href="my_images/<%- dinner_image %>" data-lightbox="image-<%= dinner_id %>" data-title="My caption"><img src="my_images/<%- dinner_image %>"></img></a> </figure><% });%>
                </div>
 
                <div class="box_inside">

                <div class="p-wrap d-flex justify-content-center">
                  <p>ID: <%= dinner_id %></p>
                  <p>餐廳名稱: <%= restaurant_id %></p>
                  <p>品名: <%= small_cat %></p>
                  <p>辣度: <%= spicy %></p>
                </div>
                
                <h3><%= name %><p>狀態：<%= onboard %></p></h3>

                <div class="d-flex">
                <p>食材：</p>
                  <%
                    _.forEach(food, function(food) {  %><p><%- food %></p><% } );
                    %>
                </div>
                <div class="d-flex">
                <p>商品名：</p>
                    <%
                    _.forEach(product, function(product) { %><p><%- product %></p><% });
                    %>
                </div>
                 
                <div class="intro transition">簡介
                <div><%= intro %></div>
                </div>
                </div>
            </div>`;


  Notiflix.Confirm.Init({
      width: "350px",
      okButtonBackground: "#ce4e4e",
      titleColor: "#e81616",
      titleFontSize: "20px",
      fontFamily: "Arial",
      useGoogleFont: false,
  });

  // function edit_conf(sid){
  //   fetch('dinner_edit.php?sid='+sid)
  // };

  function delete_conf(sid){
        Obj.sid = sid;

        // console.log(sid);
        //觸發函式
        Notiflix.Confirm.Show(
            // Notice Content
            '! WARNING !',
            '確定刪除嗎 ?',
            '刪除',
            '取消',
            // ok button callback

            function () {
              loadData(Obj.page, Obj.sid, change);
            },

            // cancel button callback
            function() {
              console.log(Obj.sid);
            }
        );
    
  }

    function changeSort(){
      change = !change;
      number = change? 1 : 0;
      // console.log(number);
       loadData(Obj.page, Obj.sid, Obj.change);
       let i = number? '<i class="fas fa-sort-down"></i>':'<i class="fas fa-sort-up"></i>';
       change_i.innerHTML = i;
    };

    const pagination_fn = _.template(pagination_str);
    const table_row_fn = _.template(table_row_str);
    // const table_food_fn = _.template(table_food_str);
    const test_str_fn = _.template(test_str);
    

    function loadData (i){
        toggle = true;
        Obj.page = i;
        if (Obj.sid>0) {
          fetch('dinner_delete.php?sid='+Obj.sid)
          .then(response=>{
            return response.json();
          })
          .then(jsonObj=>{
            console.log(jsonObj);
            // alert(jsonObj['success']);
            loadData();
            Obj.sid = 0;

            // Notiflix.Confirm.Show(
            //   // Notice Content
            //   '這是通知',
            //   `${jsonObj['success']}`,
            //   '回菜色列表',
            //   '新增菜色',

            //   function () {
            //     location.href("dinner_list.php")
            //   },

            //   function() {
            //     location.href("dinner_insert.php")
            //   }
            // );

            // setTimeout(() => {
            //   location.href="dinner_list.php";
            // }, 500);
          })
        }
        // console.log(Obj.searchName);
    
        // 搜尋功能
        fetch(`dinner_list_API.php?page=${Obj.page}&number=${number}&searchName=${Obj.searchName}&searchType=${Obj.searchType}`)
          .then(response => {
              return response.json();
          })
          .then(jsonObj => {
            console.log(jsonObj);
            $('#rowCount').text(`資料總筆數：${jsonObj.totalRows}`);

            if(jsonObj['search']){
              Notiflix.Confirm.Show(
                  // Notice Content
                  '! WARNING !',
                  `${jsonObj['search']}`,
                  '回列表',
                  '新增菜色',
                  // ok button callback

                  function () {
                    // location.href="dinner_list.php"
                    $('#search').val('');
                    Obj.searchName = '';
                    Obj.searchType = '';
                    loadData();
                  },
                  // cancel button callback
                  function() {
                    location.href="dinner_insert.php"
                  }
              );
            }
        
              let i, s, item;
              let t_str = '';
            //   let t_str_f = '';
                  for(s in jsonObj.rows){
                      item = jsonObj.rows[s];
                        let other_ingred = JSON.parse(item['other_ingred']);
                        for(s in other_ingred){
                            // console.log(other_ingred[s]);
                            other_ingred[s] = vice_food[other_ingred[s]];
                          }

                        item.other_ingred = other_ingred;

                        // console.log(item.dinner_image);
                        new_img = JSON.parse(item['dinner_image']);
                        item.dinner_image = new_img;
                        // console.log(item.main_ingred);

                      t_str += table_row_fn(item);
                  }

          

                //   for(s in jsonObj.food_ingred){
                //     food = jsonObj.food_ingred[s];
                //     console.log(food);
                //     t_str_f += table_food_fn(food);
                //     console.log(t_str_f);
                // }

                  card_content.innerHTML = big_table;

                  const t_content = document.querySelector('#t_content');

                  t_content.innerHTML = t_str;

              let p_str = '';
                  for(i=1; i<=jsonObj.totalPages; i++){
                    let active = i===jsonObj.page ? '' : '';
                    p_str += pagination_fn({i:i, active:active});
                  }
                  pagination.innerHTML = p_str;
            
          // 多選功能
            
            let checked_check = [];
            let new_check='';  
            let onboard = $('.my_choose');
            let on = 0;
            
            function go(e){
                            // console.log(e)
                            if(e.target.checked){
                                  checked_check.push(e.target.value)
                            }else{
                                let index = checked_check.indexOf(e.target.value)
                                checked_check.splice(index, 1)
                            }
                            // console.log(checked_check)
                            new_check = JSON.stringify(checked_check);
                            
                            $('#push').one('click', on_func);
                            $('#push').one('click', fet_1);
                            
                            $('#drop').one('click', off_func);
                            $('#drop').one('click', fet_1);
                        

                              function on_func(){
                                on = 0;
                              }

                              function off_func(){
                                on = 1;
                              }
                              
                              function fet_1 (e){
                                console.log(checked_check);
                                console.log(new_check)
                                fetch(`dinner_list_API.php?checked_check=${new_check}&on=${on}`)
                                .then(response => {
                                  loadData();
                                  checked_check=[];
                                  new_check='';
                                })

                                let dom = $(this).data('dom');
                                $(this).text(`選取項目${dom}`);
                                $(this).off('click', fet_1);
                              }               
                }
                 
              $('#push').click(function(){
                  $(this).text('上架');
                 
                  $('#checkTotal').removeAttr('disabled');
                  $('.my_choose').removeAttr('disabled');

                  $('.my_choose').on('change', go);
              })

              $('#drop').click(function(){
                  $(this).text('下架');

                    
                  $('#checkTotal').removeAttr('disabled');
                  $('.my_choose').removeAttr('disabled');

                  $('.my_choose').on('change', go);
              })        
                     
            
      })
    }

    loadData();


    function test (i){
        toggle = false;
        Obj.page = i;
        if (Obj.sid>0) {
          fetch('dinner_delete.php?sid='+Obj.sid)
          .then(response=>{
            return response.json();
          })
          .then(jsonObj=>{
            console.log(jsonObj);
            // alert(jsonObj['success']);
            test();
            Obj.sid = 0;
            // Notiflix.Confirm.Show(
            //   // Notice Content
            //   '這是通知',
            //   `${jsonObj['success']}`,
            //   '回菜色列表',
            //   '新增菜色',

            //   function () {
            //     location.href("dinner_list.php")
            //   },

            //   function() {
            //     location.href("dinner_insert.php")
            //   }
            // );

            // setTimeout(() => {
            //   location.href="dinner_list.php";
            // }, 500);
          })
        }
        // console.log(Obj.searchName);
        fetch(`dinner_list_API.php?page=${Obj.page}&number=${number}&searchName=${Obj.searchName}&searchType=${Obj.searchType}`)
          .then(response => {
              return response.json();
          })
          .then(jsonObj => {
            console.log(jsonObj);

            Obj.searchType = '';
            Obj.searchName = '';

            if(jsonObj['search']){
              Notiflix.Confirm.Show(
                  // Notice Content
                  '! WARNING !',
                  `${jsonObj['search']}`,
                  '回列表',
                  '新增菜色',
                  // ok button callback

                  function () {
                    // location.href="dinner_list.php"
                    $('#search').val('');
                    Obj.searchName = '';
                    Obj.searchType = '';
                    loadData();
                  },
                  // cancel button callback
                  function() {
                    location.href="dinner_insert.php"
                  }
              );
            }
        
              let i, s, item;
              let t_str = '';
            //   let t_str_f = '';
                  for(s in jsonObj.rows){
                      item = jsonObj.rows[s];
                        // console.log(item.dinner_image);
                        new_img = JSON.parse(item['dinner_image']);
                        item.dinner_image = new_img;
                        // console.log(item.main_ingred);

                      t_str += test_str_fn(item);
                  }

          

                //   for(s in jsonObj.food_ingred){
                //     food = jsonObj.food_ingred[s];
                //     console.log(food);
                //     t_str_f += table_food_fn(food);
                //     console.log(t_str_f);
                // }

                  card_content.innerHTML = t_str;

              let p_str = '';
                  for(i=1; i<=jsonObj.totalPages; i++){
                    let active = i===jsonObj.page ? '' : '';
                    p_str += pagination_fn({i:i, active:active});
                  }
                  pagination.innerHTML = p_str;

              // 多選上下架
              // console.log(onboard)
              let id = 0;
              let checked_check = [];          
              let on = 0;
       
                  
                  // 點擊選取 btn 後再啟動點擊 box 事件
                  function go(){


                    // card 多選的部分
                        id = $(this)[0].id;
                        $(this).toggleClass('active');
                        $(this).addClass('jello');
                        $(this).find('p').toggleClass('active');
                        let match = $(this).hasClass('active');
                        // console.log(match);

                          if(match){                 
                              checked_check.push(id)
                          }else{
                              let index = checked_check.indexOf(id)
                              checked_check.splice(index, 1)
                          }
                           console.log(checked_check)
                          new_check = JSON.stringify(checked_check);
                          console.log(new_check)
                 
                          $('#push').one('click', on_func);
                          // push.addEventListener('click', fet);
                          $('#push').one('click', fet);
                          
                          $('#drop').one('click', off_func);
                          // drop.addEventListener('click', fet);
                          $('#drop').one('click', fet);
                       
                            function on_func(){
                              on = 0;
                            }

                            function off_func(){
                              on = 1;
                            }
                            
                            function fet(){
                                console.log(on)
                                fetch(`dinner_list_API.php?checked_check=${new_check}&on=${on}`)
                                .then(response => {
                                  test();
                                  checked_check=[];
                                  new_check='';
                                })
                            }

                    }
                         
              $('#push').click(function(){
                  $(this).text('上架');
                  $('.card').on('click', '.box', go);
              })

              $('#drop').click(function(){
                  $(this).text('下架');
                  $('.card').on('click', '.box', go);
              })

          
      })
    }

    // 切換呈現的頁面後，個別保留搜尋功能
    document.querySelector('#search').addEventListener('change', function(e){
    // console.log(e.target.value);
      if(toggle){
          Obj.searchName = e.target.value;
          loadData(Obj.searchName);
      }else{
          Obj.searchName = e.target.value;
          test(Obj.searchName);
      }
    })
 
    $('.card').on('click', '.intro', function(e){
        e.stopPropagation();
        $(this).toggleClass('active')
    })


    // let container = document.querySelector('.main-panel')

    $('.main-panel').on('ps-scroll-y', function(){
           $('.image_wrap').addClass('active');
    });

    // 全選功能
    let checkNum = 0;

    $('.card').on('change', '#checkTotal', function(){
        let check = $(this).prop('checked');
        $('tbody :checkbox').prop('checked', check);
        let checkOrNot = $('tbody :checkbox').prop('checked');
        if(checkOrNot){
          $('tbody :checkbox').parents('tr').css('background', 'rgba(255,150,150,0.6)');
        }else{
           $('tbody :checkbox').parents('tr').css('background', 'none');
        }
    })

    $('.card').on('change', 'tbody :checkbox', function(){
        let checkOrNot = $(this).prop('checked');
        if(checkOrNot){
          $(this).parents('tr').css('background', 'rgba(255,150,150,0.6)');
        }else{
           $(this).parents('tr').css('background', 'none');
        }
        
        checkNum = $('tbody :checkbox').length;
        let checkedNum = $('tbody :checked').length;
        if(checkNum == checkedNum){
          $('#checkTotal').prop('checked', true);
        }else{
          $('#checkTotal').prop('checked', false);
        }
    })

    // 中、西式 分類篩選
    $('.type').on('click', function(){
      Obj.searchType = $(this).data('type');
      if(toggle){
          loadData(Obj.searchType);
      }else{
          test(Obj.searchType);
      }
    })


    // 卡牌、表格呈現方式切換
    $('.show').on('click', function(){

        // 歸零 (按了之後可以回復為還沒搜尋的資料)
        Obj.searchType = '';
        Obj.searchName = '';
        $('#search').val('');

      show = $(this).data('show');
      if(show=='card'){
        test();
      }else{
        loadData();
      }
    })


    // 換頁並維持呈現方式
    $('.pagination').on('click', '#pageLink', function(e){
                e.preventDefault();
                // Obj.searchType = $(this).data('type');
                Obj.page = e.target.dataset.page;
                console.log(Obj.page)
                if(toggle){
                    loadData( Obj.page);
                }else{
                    test( Obj.page);
                }
     })  


</script>


<?php include '../__html_foot.php' ?>