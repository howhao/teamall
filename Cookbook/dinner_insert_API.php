<?php


    require '../__connect_db.php';


    $restaurant_id = $_POST['restaurant_id'];

    // 假的
    // $restaurant_id = rand(1, 20);

    $onboard = isset($_POST['onboard'])? $_POST['onboard'] : '';

    // 拿菜色分類 id
    $small_cat = isset($_POST['small_cat'])? $_POST['small_cat'] : '';

     // 菜色名稱介紹
     $name = isset($_POST['dinner'])? $_POST['dinner'] : '';
     $intro = isset($_POST['intro'])? $_POST['intro'] : '';

     // AJAX 給 JS 的東西
     $result = [
        'status' => '新增成功',
        'info' => '感謝您的新增',
        'small_cat' => $small_cat,
        'name' => $name,
        'intro' => $intro,
        'to' => '回菜色列表',
        'orto' => '繼續新增',
        'ingre' => [],
    ];
   
    
    // 菜色名稱為必填欄位
    if(empty($name)){
        $result['status'] = '新增失敗';
        $result['info'] = '沒填名稱喔';
        $result['orto'] = '繼續填寫';
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        exit;
    };

    // if(empty($_FILES))

    // 食材 id
    $main_ingred = isset($_POST['main_ingred'])? $_POST['main_ingred'] : 0;
    $other_ingred = isset($_POST['other_ingred'])? $_POST['other_ingred'] : '';
    $other_ingred = json_encode($other_ingred, JSON_UNESCAPED_UNICODE);
   
    $result['ingre'] = $main_ingred;
    // echo json_encode($result, JSON_UNESCAPED_UNICODE);
    // exit;
  
    // 食材 必填欄位
    // if(empty($main_ingred1)){
    //     $result['status'] = '新增失敗';
    //     $result['info'] = '請選食材';
    //     $result['orto'] = '繼續填寫';
    // }

    // 菜色標籤
    $flavor = isset($_POST['flavor'])? $_POST['flavor'] : 0;
    // print_r($flavor);

    $spicy = isset($_POST['spicy'])? $_POST['spicy'] : 0;

    // exit;

    // 看拿到什麼值
    // echo "{$restaurant_id}<br>{$main_cat}<br>{$small_cat}<br>{$name}<br>{$intro}<br>{$main_ingred}<br>{$main_ingred_place?}<br>{$main_ingred_place?}<br>{$main_ingred_place?}<br>";

    // 圖片上傳部分
    $upload = __DIR__. '/my_images/';

    $picture_num = 0;

    $picture = $_FILES['picture'];
    // print_r($picture);
    $picture_name = $picture['name'];
    $picture_type = $picture['type'];
    $picture_tmp_name = $picture['tmp_name'];

    // 上傳圖片不足 2 張或超過 5 張，結束php
    if(! empty($picture_name[0])){
        $picture_num = count($_FILES['picture']['name']);
        if($picture_num > 5){
            $result['status'] = '新增失敗';
            $result['info'] = '上傳圖片超過限制數量';
            $result['orto'] = '繼續填寫';
            echo json_encode($result, JSON_UNESCAPED_UNICODE);
            exit;
        };
    }else {
        $result['status'] = '新增失敗';
        $result['info'] = '沒有上傳圖片喔';
        $result['orto'] = '繼續填寫';
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        exit;
    };

    $allow_type = [
        'image/png',
        'image/jpeg',
    ];

    $ext = [
        'image/png' => '.png',
        'image/jpeg' => '.jpg',
    ];

    $new_ext = [];

    // echo json_encode($result, JSON_UNESCAPED_UNICODE);
    // exit;

    // print_r($picture_name[?]);
 
    if(! empty($picture_name[0])){  
          // 拿檔名重新編碼 (md?)
          foreach ($picture_name as $k => $v) {
            $new_name[] = md5(uniqid().$v);          
        };
        // print_r($new_name);

        // 拿 type 給副檔名
        foreach ($picture_type as $k => $v) {
            if(in_array($v, $allow_type)){
                $new_ext[] = $ext[$v];
            };
        };
        // print_r($new_ext);

        //移動檔案位址
        foreach ($picture_tmp_name as $k => $v) {
            // echo($v);
            move_uploaded_file($v, $upload.$new_name[$k].$new_ext[$k]);
        };
    
    // exit;
 
        $image = [];
        foreach ($new_name as $k => $v) {
                $image[] = $v.$new_ext[$k];
        };
        
        // print_r($image);
        $dinner_image = json_encode($image, JSON_UNESCAPED_UNICODE);

        $result['orto'] = '新增下一筆';

        // print_r ($dinner_image);

        // exit;


       // 新增資料
        $sql_d = "INSERT INTO `dinner_list`(`dinner_id`, `restaurant_id`, `small_cat`, `name`, `other_ingred`, `intro`, `dinner_image`, `onboard`, `spicy`)
        VALUES 
        (NULL,?,?,?,?,?,?,?,?)";
    
        $stmt_d = $pdo->prepare($sql_d);
    
        $stmt_d->execute([
            $restaurant_id,
            $small_cat,
            $name,
            $other_ingred,
            $intro,
            $dinner_image,
            $onboard,
            $spicy
        ]);
    };

    $dinner_list = $pdo->lastInsertId();
     
    // echo $dinner_list;

    
    $sql= "INSERT INTO `dinnerproduct` (`sid`, `product_category`, `product_class`, `farmer_product`, `dinner_list`) 
    VALUES 
    (NULL, ?, ?, ?, ?)"; 

    $stmt = $pdo->prepare($sql);

    $num = count($main_ingred);

    for($i=0; $i<$num; $i+=3){

        $class = $main_ingred[$i];
        $food = $main_ingred[$i+1];
        $product_id = $main_ingred[$i+2];
   
        $stmt->execute(
            [$class, $food, $product_id, $dinner_list]
        );    
     
    };

    
    $sql= "INSERT INTO `tagdinner` (`sid`, `tag`, `dinner_list`) 
    VALUES 
    (NULL, ?, ?)"; 

    $stmt = $pdo->prepare($sql);

    $num = count($flavor);

    for($i=0; $i<$num; $i++){

        $tag = $flavor[$i];

        $stmt->execute(
            [$tag, $dinner_list]
        );    
     
    };

    
    echo json_encode($result, JSON_UNESCAPED_UNICODE);

?>