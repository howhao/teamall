<?php

require '../__connect_db.php';

// 菜色大分類
// $main_cat = [
//     '0' => '中式',
//     '1' => '西式',
//     '2' => '日式',
//     '3' => '泰式'
//   ];
  
  // 菜色小分類
  $small_cat = [
    '0' => '主食',
    '1' => '主菜',
    '2' => '湯',
    '3' => '甜點',
  ];

   // 菜色口味
   $flavor = [
    '2' => '全素',
    '3' => '蛋奶素',
    '6' => '當季限定',
    '10' => '紅燒',
    '11' => '清蒸',
    '12' => '涼拌',
    '13' => '酥炸',
    '14' => '熱炒',
    '15' => '白醬',
    '16' => '紅醬',
    '17' => '青醬',
    '18' => '香蒜',
    '19' => '起司',
  ];

  $spicy = [
    '0' => '不辣',
    '1' => '小辣',
    '2' => '中辣',
    '3' => '大辣',
    '4' => '特辣',
  ];

  // 副食材種類
  $other_ingred = [
    '11' => '菇類',
    '7' => '蔬果類',
    '10' => '豆類'
  ];

  $sql ="SELECT `category_sid`, `name` FROM `product_category` WHERE `parent_sid`=1";

  $stmt_c = $pdo->query($sql);
  $rows_c = $stmt_c->fetchAll(PDO::FETCH_NUM);
  // print_r($rows_c);

  $main_ingred_class = [];
  foreach ( $rows_c as $key => $value) {
    // echo ($value[0]);
    $main_ingred_class += [$value[0]=> $value[1]];
  };
    // print_r($main_ingred_class);
  
  // 食物名稱
  $sql ="SELECT `class_sid`, `name` FROM `product_class` WHERE 1";

  $stmt = $pdo->query($sql);
  $rows = $stmt->fetchAll(PDO::FETCH_NUM);
  // print_r($rows);

  $food_name = [];
  foreach ($rows as $key => $value) {
    // echo ($value[0]);
    $food_name += [$value[0]=> $value[1]];
  };
  // print_r($food_name);


  $food_num = [];
  foreach ($rows as $key => $value) {
    // echo ($value[0]);
    $food_num += [$value[1]=> $value[0]];
  };
  // print_r($food_num);


  // 抓某食材的所有商品與價格
  $sql_product ="SELECT `sid`, `class_sid`, `name`, `price`, `specification` FROM `farmer_product` WHERE 1";

  $stmt_product = $pdo->query($sql_product);
  $rows_product = $stmt_product->fetchAll(PDO::FETCH_NUM);

  $product = [];
  foreach ($rows_product as $key => $value) {
    // echo ($value[0]);
    $product[] = [$value[1]=> $value[2]];
  };
  // print_r($product);

  $product_sid = [];
  foreach ($rows_product as $key => $value) {
    // echo ($value[0]);
    $product_sid[] = [$value[0]=> $value[2]];
  };
  // print_r($product_sid);

  $product_price = [];
  foreach ($rows_product as $key => $value) {
    // echo ($value[0]);
    $product_price[] = [$value[0]=> $value[3]];
  };
  // print_r( $product_price);

  $product_specification = [];
  foreach ($rows_product as $key => $value) {
    // echo ($value[0]);
    $product_specification[] = [$value[0]=> $value[4]];
  };
  // print_r($product_specification);

   
?>