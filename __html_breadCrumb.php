<?php if ($page_name !== 'adminhome'): ?>
    <div class="col-md-5 col-lg-4 col-sm-5 mt-5 pt-4 pr-0 d-inline-block">
        <nav aria-label="breadcrumb " role="navigation">
            <ol class="breadcrumb" style="background-color: transparent">
                <li class="breadcrumb-item"><a href="../Admin/adminHome.php">Home</a></li>
                <?php if ($page_name == 'farmer_edit' or $page_name == 'farmer_create' or $page_name == 'farmer_product_edit' ): ?>
                        <li class="breadcrumb-item" aria-current="page">
                            <a href="../<?= $page_d['file'] ?>/<?= $page_d['parent'] ?>.php"><span><?=$page_d['parent_title']?></span></a>
                        </li>
                <?php endif; ?>
                <li class="breadcrumb-item active" aria-current="page" style=""><span><?= $page_title ?></span></li>
                <?php if ($page_name == 'farmer'): ?>
                    <li class="" style="padding-left: 10px;margin-top: 5px; transform: scale(1.5)">
                        <a id="layout_swich" class="" style="cursor: pointer ">
                            <i class="nc-icon nc-layout-11 ">
                            </i>
                        </a>
                    </li>
                <?php endif; ?>
            </ol>
        </nav>
    </div>
<?php endif; ?>